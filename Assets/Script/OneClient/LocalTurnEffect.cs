﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class LocalTurnEffect : MonoBehaviour
{
	public Sprite Pintos;
	public Sprite Haskell;
	
	public bool for1P;
	public Local_SelectionData selection;
	public LocalManager manager;

	void Start()
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
	}

	public void DoSetting()
	{
		if(for1P == manager.isPlayer1P(manager.Turn))
		{
			if(selection.getCurrentTeam(manager.Turn) == Teams.Pintos)
			{
				GetComponent<SpriteRenderer>().sprite = Pintos;
			}
			else
			{
				GetComponent<SpriteRenderer>().sprite = Haskell;
			}
		}
		else
		{
			GetComponent<SpriteRenderer>().sprite = null;
		}
	}
}
