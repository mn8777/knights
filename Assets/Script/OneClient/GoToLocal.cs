﻿using UnityEngine;
using System.Collections;

public class GoToLocal : MonoBehaviour
{
	public Sprite deactivatedButton;
	public Sprite activatedButton;

	public AudioClip mouseOverSE;
	public AudioClip mouseClickSE;

	public GameObject shroud;

	private SpriteRenderer sr;
	private AudioSource se;

	void Awake()
	{
		se = GetComponent<AudioSource>();
	}

	void Start()
	{
		sr = GetComponent<SpriteRenderer>();
	}

	void OnMouseDown ()
	{
		StartCoroutine("MouseClick");
	}

	void Deactivate(){
		sr.sprite = deactivatedButton;
	}

	void Activate(){
		sr.sprite = activatedButton;
	}

	IEnumerator MouseClick()
	{
		AllButtonManagerAtTitle.GetAllButtonManagerInstance().AllButtonColliderDeactive();
		shroud.SetActive(true);
		iTween.FadeFrom(shroud,0f,0.7f);
		se.PlayOneShot(mouseClickSE, 1);
		yield return new WaitForSeconds(1);
		Application.LoadLevel ("LocalTeamSelect1");
	}

	void OnMouseEnter ()
	{
		se.PlayOneShot(mouseOverSE, 1);
		Activate();
	}

	void OnMouseExit()
	{
		Deactivate();
	}

//	void OnMouseDown()
//	{
//		Application.LoadLevel ("LocalTeamSelect1");
//		checker.isLocal = true;
//	}
}