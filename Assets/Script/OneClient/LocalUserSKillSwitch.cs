﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class LocalUserSKillSwitch : MonoBehaviour
{
	private SpriteRenderer sr;

	public bool for1P;

	public Sprite Pintos;
	public Sprite Haskell;

	public Local_SelectionData selection;

	void Start()
	{
		sr = GetComponent<SpriteRenderer> ();
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		SwitchTeam (GetTeam(for1P));
	}

	Teams GetTeam(bool is1P)
	{
		if (is1P)
			return selection.team1;
		else
			return selection.team2;
	}

	void SwitchTeam (Teams team)
	{
		if(team == Teams.Pintos)
		{
			sr.sprite = Pintos;
		}
		else
		{
			sr.sprite = Haskell;
		}
	}
}
