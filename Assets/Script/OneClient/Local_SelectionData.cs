using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;
using CommonConstants;

public class Local_SelectionData : MonoBehaviour {
	public Teams team1;
	public Teams team2;
	public int currentPlayer;
	public Teams currentTeam;
	public int currentSelection;
	public int[] character1;
	public int[] character2;

	void Start()
	{
		DontDestroyOnLoad (gameObject);
		currentPlayer = 1;
		for (int i = 0; i<Constants.ofTeamMembers; i++) {
			character1 [i] = 10;
			character2 [i] = 10;
		}
	}

	public int[] characters(int player)
	{
		if (player == 1)
			return character1;
		else
			return character2;
	}

	public Teams getCurrentTeam (int player)
	{
		if (player == 1)
			return team1;
		else
			return team2;
	}
}
