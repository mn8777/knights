﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class LocalCharacterInfoSheetInGameUI : MonoBehaviour
{
	public Sprite[] InfoSheets;
	public Sprite[] Background;
	public static int viewedInfoSheet=Constants.ofCharactersInEachTeam * 2;
	private int[] selectedCharacter = new int[Constants.ofTeamMembers*2];
	public int xAxisforPlayer1;
	public int xAxisforPlayer2;
	public int zAxis;
	private GameObject background;
	public LocalManager manager;
	
	private SpriteRenderer sr;
	private SpriteRenderer backgroundSr;

	void Start ()
	{
		background = GameObject.Find ("/InfoSheet/Background");
		backgroundSr = background.GetComponent<SpriteRenderer> ();
		backgroundSr.sprite = Background [0];
		sr = GetComponent<SpriteRenderer>();
	}

	void Update ()
	{
		if(viewedInfoSheet<Constants.ofCharactersInEachTeam*2)
		{
			sr.sprite=InfoSheets[selectedCharacter[viewedInfoSheet]];
			backgroundSr.sprite=Background[1];
			
			if(viewedInfoSheet<Constants.ofTeamMembers)
			{
				transform.position=new Vector3(xAxisforPlayer1,40-9*viewedInfoSheet,zAxis);
			}
			else if (viewedInfoSheet>=Constants.ofTeamMembers)
			{
				transform.position=new Vector3(xAxisforPlayer2,76-9*viewedInfoSheet,zAxis);
			}
			
		}
		else if(viewedInfoSheet==Constants.ofCharactersInEachTeam*2)
		{
			sr.sprite=InfoSheets[20];
			backgroundSr.sprite = Background [0];
		}
	}

	public void SetCharacterNames ()
	{
		for (int i = 0; i < 2 * Constants.ofTeamMembers; i++)
		{
			selectedCharacter [i] = manager.Characters[i].CharacterIndex;
		}
	}
}
