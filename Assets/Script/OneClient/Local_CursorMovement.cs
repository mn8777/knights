﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class Local_CursorMovement : MonoBehaviour
{
	public Local_SelectionData selection;

	private	float cursorXPosition;
	private float cursorYPosition;

	void Start ()
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		cursorYPosition = transform.position.y;
	}

	void Hide ()
	{
		transform.position = new Vector3 (0.0f, 0.0f, 1000.0f);
	}

	void Update ()
	{
		if(selection.currentSelection<Constants.ofCharactersInEachTeam)
		{
			if(Mathf.Abs(cursorXPosition-(selection.currentSelection * 0.97f - 1.85f))>0.1f){
				audio.Play();
			}
			cursorXPosition = (selection.currentSelection * 0.97f - 1.85f);
			transform.position= new Vector3(cursorXPosition,cursorYPosition,-1.0f);
		}
		else
		{
			Hide ();
		}
	}
}