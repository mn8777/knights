using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;

public class Local_TeamSelect : MonoBehaviour
{
	public Teams team;
	public Local_SelectionData manager;

	void Start()
	{
		manager = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		gameObject.renderer.material.color = Color.gray;
	}

	void SelectTeam(int player, Teams teamName)
	{
		if(player == 1)
		{
			manager.team1 = teamName;
			manager.currentTeam = teamName;
		}
		else
		{
			manager.team2 = teamName;
			manager.currentTeam = teamName;
		}
	}

	void OnMouseOver()
	{
		gameObject.renderer.material.color = Color.white;
	}

	void OnMouseExit ()
	{
		gameObject.renderer.material.color = Color.gray;
	}

  	void OnMouseDown()
  	{
		if(team == Teams.Pintos)
		{
			SelectTeam(manager.currentPlayer, Teams.Pintos);
		}
		else
		{
			SelectTeam(manager.currentPlayer, Teams.Haskell);
		}
		Application.LoadLevel ("LocalCharacterSelect");
	}
}
