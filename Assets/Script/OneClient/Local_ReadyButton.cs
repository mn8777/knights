﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class Local_ReadyButton : MonoBehaviour
{
	public Local_SelectionData selection;
	public Local_SelectedCharacters selected;
	public Sprite deactivatedReadyButton;
	public Sprite activatedReadyButton;
	public Sprite clickedReadyButton;
	public Sprite deactivatedStartButton;
	public Sprite activatedStartButton;
	public Sprite clickedStartButton;
	private SpriteRenderer sr;

	bool clicked;

	void FinishSelect()
	{
		if(selection.currentPlayer == 1)
		{
			selection.currentPlayer++;
			Application.LoadLevel("LocalTeamSelect2");
		}
		else
		{
			CS_WaitingImage.CallWaitingImage();
			Application.LoadLevel("LocalInGame");
		}
	}

	void Start ()
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		selected = GameObject.FindObjectOfType (typeof(Local_SelectedCharacters)) as Local_SelectedCharacters;
		sr = GetComponent<SpriteRenderer> ();
		Deactivate();
		clicked = false;
	}

	void Update()
	{
		if (Local_ReadyCounter.readyCount == Constants.ofTeamMembers)
			Activate ();
		else
			Deactivate ();
	}
	
	void OnMouseDown()
	{
		Clicked ();
		if(Local_ReadyCounter.readyCount==Constants.ofTeamMembers)
		{
			FinishSelect();
		}
	}

	void Deactivate()
	{
		if(selection.currentPlayer == 1)
		{
			sr.sprite = deactivatedReadyButton;
		}
		else
		{
			sr.sprite = deactivatedStartButton;
		}
		gameObject.collider.enabled=false;
	}
	
	void Activate()
	{
		if(selection.currentPlayer == 1)
		{
			sr.sprite = activatedReadyButton;
		}
		else
		{
			sr.sprite = activatedStartButton;
		}
		gameObject.collider.enabled=true;
	}

	void Clicked(){
		clicked = true;
		audio.Play ();
		if (selection.currentPlayer == 1)
		{
			sr.sprite = clickedStartButton;
		} 
		else
		{
			sr.sprite= clickedReadyButton;
		}
		gameObject.collider.enabled=false;
	}
}
