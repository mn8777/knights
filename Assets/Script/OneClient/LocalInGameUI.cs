﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class LocalInGameUI : MonoBehaviour
{
	public bool for1P;
	
	public GameObject[] HpBars;
	
	public SpriteRenderer ShowTeam;
	public Sprite Pintos;
	public Sprite Haskell;
	
	public Sprite[] Portraits;
	
	public SpriteRenderer[] PortraitRenderers;

	public LocalManager manager;
	public Local_SelectionData selection;
	
	void Start()
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		if(for1P)
		{
			if(selection.team1 == Teams.Pintos)
			{
				ShowTeam.sprite = Pintos;
			}
			else
			{
				ShowTeam.sprite = Haskell;
			}
		}
		else
		{
			if(selection.team2 == Teams.Pintos)
			{
				ShowTeam.sprite = Pintos;
			}
			else
			{
				ShowTeam.sprite = Haskell;
			}
		}
	}
	
	public void SetCharacterNames ()
	{
		for (int i = 0; i < Constants.ofTeamMembers; i++)
		{
			if (Portraits [manager.Characters [manager.ReturnCharacterCount_IfPlayer2 (for1P) + i].CharacterIndex] != null) 
			{
				PortraitRenderers [i].sprite = Portraits [manager.Characters [manager.ReturnCharacterCount_IfPlayer2 (for1P) + i].CharacterIndex];
			}
		}
	}
	
	void Update()
	{
		for(int i = 0; i < Constants.ofTeamMembers; i++)
		{
			LocalCharacter Knight = manager.Characters[manager.ReturnCharacterCount_IfPlayer2 (for1P)+i];
			Vector3 Scale = HpBars[i].transform.localScale;
			
			if(Knight == null)
			{
				Scale.x = 0;
			}
			else if(Knight.MaxHP != 0)
			{
				Scale.x = HpRatio(Knight) * Constants.ScaleOfHpBar;
			}
			
			HpBars[i].transform.localScale = Scale;
		}
	}
	
	float HpRatio(LocalCharacter Knight)
	{
		return (float)(Knight.HP) / (float)(Knight.MaxHP);
	}
}
