using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class Local_EachSelectedCharacters : MonoBehaviour
{
	
	public Sprite[] pintos;
	public Sprite[] haskell;
	public int slot;
	public Local_SelectionData selection;
	public Local_ReadyCounter counter;
	public Local_SelectedCharacters selected;
	public GameObject[] characters;
	public GameObject canceledCharacter;
	Animator anim;
	int select = -1;
	
	private SpriteRenderer sr;

	void Start ()
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		sr = GetComponent<SpriteRenderer>();
		sr.sprite = pintos [Constants.ofCharactersInEachTeam];
		anim = GetComponent<Animator> ();
	}

	void Update () 
	{
		if(select != selected.slot[slot])
		{
			if (selection.getCurrentTeam (selection.currentPlayer) == Teams.Haskell)
			{
				sr.sprite = haskell [selection.characters (selection.currentPlayer) [slot]];
			}
			else if (selection.getCurrentTeam (selection.currentPlayer) == Teams.Pintos)
			{
				sr.sprite = pintos [selection.characters (selection.currentPlayer) [slot]];
			}
			select = selected.slot[slot];
			anim.SetTrigger("selected");
		}
	}
	
	void OnMouseDown() 
	{
		canceledCharacter = characters[selection.characters(selection.currentPlayer)[slot]];
		LocalCharacterSelect select = canceledCharacter.GetComponent<LocalCharacterSelect> ();
		select.activate = true;
		selected.clickedSelectedCharacter[slot]=true;
		anim.SetTrigger ("diselected");
	}
}