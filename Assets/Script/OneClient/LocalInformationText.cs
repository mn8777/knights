﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class LocalInformationText : MonoBehaviour
{
	public TextMesh Text;
	
	public LocalManager Manager;
	
	public bool isPlayer1P;

	public void SetCharacterNames()
	{
		if(Manager.Characters.Count == Constants.ofTeamMembers*2)
		{
			Text.text = Manager.Characters[Manager.ReturnCharacterCount_IfPlayer2(isPlayer1P)].name;
			
			for(int i = 1; i<Constants.ofTeamMembers; i++)
			{
				Text.text += "\n\n\n"+Manager.Characters[Manager.ReturnCharacterCount_IfPlayer2(isPlayer1P)+i].name;
			}
		}
	}
}
