﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class LocalNoticeMessage : MonoBehaviour
{
	public Sprite GameStart;
	public Sprite Pintos1P;
	public Sprite Pintos2P;
	public Sprite Haskell1P;
	public Sprite Haskell2P;
	public Sprite GameEnd;
	public LocalManager manager;
	public Local_SelectionData selection;
	public BGMManager manager_b;
	
	void Start()
	{
		manager = GameObject.FindObjectOfType (typeof(LocalManager)) as LocalManager;
		selection = GameObject.FindObjectOfType(typeof(Local_SelectionData)) as Local_SelectionData;
		manager_b = GameObject.FindObjectOfType (typeof(BGMManager)) as BGMManager;
		iTween.MoveTo(gameObject, iTween.Hash ("position", new Vector3(26.3f, 26.3f, -4), "Speed", Constants.MessageSpeed, "easeType", "linear", "oncompletetarget", gameObject, "oncomplete", "Restart"));
	}
	
	IEnumerator Restart()
	{
		yield return new WaitForSeconds (1);
		
		iTween.MoveTo(gameObject, iTween.Hash ("position", new Vector3(116.3f, 26.3f, -4), "Speed", Constants.MessageSpeed, "easeType", "linear", "oncompletetarget", gameObject, "oncomplete", "DestroyMyself"));
	}
	
	void DestroyMyself()
	{
		if(GetComponent<SpriteRenderer>().sprite == GameEnd)
		{
//			SomethingForResetVariables
			Destroy (selection.gameObject);
			Destroy (manager.gameObject);
			Destroy (manager_b.gameObject);
			Application.LoadLevel("TitleScene");
		}
		else if(GetComponent<SpriteRenderer>().sprite == GameStart)
		{
			manager.ShowTurnMessage();
		}
		
		Destroy (gameObject);
	}
}
