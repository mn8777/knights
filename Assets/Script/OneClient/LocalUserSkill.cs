﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class LocalUserSkill : MonoBehaviour
{
	public static AudioSource SE = null;
	
	public static AudioClip pintosUserSkillSound = null;
	public static AudioClip haskellUserSkillSound = null;
	public static AudioClip hitSound = null;
	
	public GameObject[] userskills;
	private GameObject userskillInstance;

	public bool for1P;
	public LocalManager manager;
	
	void Awake()
	{
		SE = GetComponent<AudioSource>();
		pintosUserSkillSound = (AudioClip)Resources.Load("explosion");
		haskellUserSkillSound = (AudioClip)Resources.Load("twinkle2");
		hitSound = (AudioClip)Resources.Load("punch");
	}

	void Start()
	{
		manager = FindObjectOfType (typeof(LocalManager)) as LocalManager;
	}

	IEnumerator OnMouseDown()
	{
		if(manager.isPlayer1P(manager.Turn) == for1P && manager.GlobalTurnPhase == LocalManager.TurnPhases.Wait)
		{
			int SkillPower = 0;
			
			if(manager.GetSkillChargeOfPlayer(manager.Turn) >= 100 && manager.GetSkillChargeOfPlayer(manager.Turn) != 200)
			{
				SkillPower = 1;
			}
			else if(manager.GetSkillChargeOfPlayer(manager.Turn) == 200)
			{
				SkillPower = 2;
			}
			//somethingForNull?
			if(SkillPower != 0)
			{
				foreach(LocalCharacter Knight in manager.Characters)
				{
					if(manager.currentTeam(manager.Turn) == Teams.Pintos && !Knight.validCharacter(manager.Turn) && Knight != null)
					{
						//						GameManager.ApplyDamage(Knight, 40*SkillPower);
						userskillInstance = Instantiate(userskills[0]) as GameObject;
						SE.PlayOneShot(pintosUserSkillSound, 1.0F);
						manager.ApplyDamage(Knight, 40*SkillPower);
						SE.PlayOneShot(hitSound, 1.0F);
						//ApplyPintosUserSkill(Knight, SkillPower);
					}
					else if(manager.currentTeam(manager.Turn) == Teams.Haskell && Knight.validCharacter(manager.Turn) && Knight != null)
					{
						//						GameManager.ApplyHaskellBuff(Knight, SkillPower);
						userskillInstance = Instantiate(userskills[1],Knight.transform.position,Quaternion.identity) as GameObject;
						SE.PlayOneShot(haskellUserSkillSound, 1.0F);
						manager.ApplyHaskellBuff(Knight, SkillPower);
						Knight.wing.sr.sprite = Knight.wing.Wings[1];
						//ApplyHaskellUserSkill(Knight, SkillPower);
					}
				}
			}

			DecreaseSkillCharge(manager.Turn, SkillPower);

		}
		
		yield return new WaitForEndOfFrame ();
		
		if(manager.aliveCountForPlayer(1) == 0 || manager.aliveCountForPlayer(2) == 0)
		{
			LocalNoticeMessage Message = Instantiate (manager.MessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as LocalNoticeMessage;
			Message.GetComponent<SpriteRenderer> ().sprite = Message.GameEnd;
		}
	}

	public void DecreaseSkillCharge(int player, int power)
	{
		if(player == 1)
			manager.userSkillCharge1 -= 100*power;
		else
			manager.userSkillCharge2 -= 100*power;
	}
}
