using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class Local_SwitchCharacter : MonoBehaviour
{
	public Sprite[] pintos;
	public Sprite[] haskel;
	public Local_SelectionData selection;
	public Teams teamName;
	
	private SpriteRenderer sr;
	void Start ()
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		sr = GetComponent<SpriteRenderer>();
		teamName = selection.currentTeam;
	}
	
	Sprite[] GetSpritesByTeam(Teams team)
	{
		if (team == Teams.Pintos)
		{
			return pintos;
		}
		else
		{
			return haskel;
		}
	}

	void Update ()
	{
		var sprites = GetSpritesByTeam (teamName);
		if (selection.currentSelection < Constants.ofCharactersInEachTeam)
		{
			sr.sprite = sprites [selection.currentSelection];
		}
	}
}