using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;
using CommonConstants;

public class LocalCharacter : LocalInteractableObject
{
	public int Attack;
	public int Move;

	public int CharacterIndex;

	public int[] IndexHP;
	public int[] IndexAttack;
	public int[] IndexMove;

	public List<Vector2> AttackableTilesForAcht;
	public List<Vector2> AttackableTilesForBlitz;
	public List<Vector2> AttackableTilesForJulius;
	public List<Vector2> AttackableTilesForMika;
	public List<Vector2> AttackableTilesForNaura;
	public List<Vector2> AttackableTilesForPy;
	public List<Vector2> AttackableTilesForRuby;
	public List<Vector2> AttackableTilesForSera;
	public List<Vector2> AttackableTilesForSesto;
	public List<Vector2> AttackableTilesForZen;
	public List<Vector2> AttackableTilesForAiry;
	public List<Vector2> AttackableTilesForAnna;
	public List<Vector2> AttackableTilesForArkadi;
	public List<Vector2> AttackableTilesForCaml;
	public List<Vector2> AttackableTilesForMichelle;
	public List<Vector2> AttackableTilesForRanard;
	public List<Vector2> AttackableTilesForRoshanak;
	public List<Vector2> AttackableTilesForSchnell;
	public List<Vector2> AttackableTilesForSuyeon;
	public List<Vector2> AttackableTilesForTigres;

	public List<List<Vector2>> AttackableTileLists = new List<List<Vector2>>();

	public bool Activated;
	public bool Moving;

	public LocalManager manager;

	public int ObjectBuffDuration;

	public Directions Direction;

	public int GameCharacterIndex;

	public LocalManager.TurnPhases TurnPhase;

	public GameObject[] AttackEffects;

	public AudioClip[] AttackSounds;
	public AudioClip HitSound;
	public AudioSource SoundEffect;

	public LocalTile StartTile;
	public Directions startDirection;
	public float startScaleX;
	
	public List<LocalInteractableObject> HitObjects;

	public GameObject Effect;

	public LocalCharacterPart body;
	public LocalCharacterPart leftLeg;
	public LocalCharacterPart rightLeg;

	public ShowIfCurrentPlayer ring;
	public WingControl wing;

	public bool isAlreadyAttacked;

	void Awake()
	{
		SoundEffect = GetComponent<AudioSource>();
	}

	void Start()
	{
		isAlreadyAttacked = false;

		manager = GameObject.FindObjectOfType (typeof(LocalManager)) as LocalManager;
		MaxHP = Constants.TickHP * IndexHP [CharacterIndex] + Constants.StandardHP;

		SetCurrentHP ();

		Attack = Constants.TickAttack * IndexAttack [CharacterIndex] + Constants.StandardAttack;
		Move = IndexMove [CharacterIndex];

		AttackableTileLists.Add (AttackableTilesForAcht);
		AttackableTileLists.Add (AttackableTilesForBlitz);
		AttackableTileLists.Add (AttackableTilesForJulius);
		AttackableTileLists.Add (AttackableTilesForMika);
		AttackableTileLists.Add (AttackableTilesForNaura);
		AttackableTileLists.Add (AttackableTilesForPy);
		AttackableTileLists.Add (AttackableTilesForRuby);
		AttackableTileLists.Add (AttackableTilesForSera);
		AttackableTileLists.Add (AttackableTilesForSesto);
		AttackableTileLists.Add (AttackableTilesForZen);
		AttackableTileLists.Add (AttackableTilesForAiry);
		AttackableTileLists.Add (AttackableTilesForAnna);
		AttackableTileLists.Add (AttackableTilesForArkadi);
		AttackableTileLists.Add (AttackableTilesForCaml);
		AttackableTileLists.Add (AttackableTilesForMichelle);
		AttackableTileLists.Add (AttackableTilesForRanard);
		AttackableTileLists.Add (AttackableTilesForRoshanak);
		AttackableTileLists.Add (AttackableTilesForSchnell);
		AttackableTileLists.Add (AttackableTilesForSuyeon);
		AttackableTileLists.Add (AttackableTilesForTigres);
	}

	public bool validCharacter(int turn)
	{
		if(turn == 1)
		{
			if(manager.isCharacter1P(this))
				return true;
			else
				return false;
		}
		else
		{
			if(manager.isCharacter1P(this))
				return false;
			else
				return true;
		}
	}

	void OnMouseDown()
	{
		if(Activated && validCharacter(manager.Turn))
		{
			if(TurnPhase == LocalManager.TurnPhases.Wait && manager.MovingCharacter == null)
			{
				TurnPhase = LocalManager.TurnPhases.Move;
				manager.GlobalTurnPhase = LocalManager.TurnPhases.Move;
				Moving = true;

				manager.MovingCharacter = this;

				GetComponent<Animator>().SetTrigger("Select");

				ShowMovableTilesOfMovingCharacter();
				manager.DeactivateLocalCharacterColliders();
			}
		}
	}

	void ShowMovableTilesOfMovingCharacter()
	{
		manager.DeactivateAllTiles();

		List<LocalTile> MovableTiles = manager.MovingCharacter.MovableRange();

		foreach(LocalTile MovableTile in MovableTiles)
		{
			if(MovableTile != null)
			{
				MovableTile.MyRenderer.sprite = MovableTile.MovableSprite;
				MovableTile.Movable = true;
			}
		}
	}

	void OnMouseEnter()
	{
		if(TurnPhase == LocalManager.TurnPhases.Wait)
		{
			List<LocalTile> TileList = ShowAndReturn_AttackRange();
			foreach(LocalTile MovableTile in MovableRange())
			{
				if(MovableTile != null)
				{
					if(MovableTile.MyRenderer.sprite == MovableTile.AttackableSprite)
					{
						MovableTile.MyRenderer.sprite = MovableTile.MovableAndAttackableSprite;
					}
					else
					{
						MovableTile.MyRenderer.sprite = MovableTile.MovableSprite;
					}
				}
			}
		}
	}

	void OnMouseExit()
	{
		if(TurnPhase == LocalManager.TurnPhases.Wait)
		{
			manager.DeactivateAllTiles();

			if(manager.GlobalTurnPhase == LocalManager.TurnPhases.Move)
			{
				ShowMovableTilesOfMovingCharacter();
			}
		}
	}

	List<LocalTile> MovableRange()
	{
		List<LocalTile> MoveRange = new List<LocalTile> ();

		for(int i = 1; i <= Move; i++)
		{
			foreach(Vector2 MovableTile in manager.MoveLevels[i-1])
			{
				MoveRange.Add(manager.GetTileAt(CurrentTile.X+(int)(MovableTile.x), CurrentTile.Y+(int)(MovableTile.y)));
			}
		}

		return MoveRange;
	}

	void Update()
	{
		if(TurnPhase == LocalManager.TurnPhases.Move && manager.GlobalTurnPhase == LocalManager.TurnPhases.Move)
		{
			if(Input.GetMouseButtonDown(1) && !GetComponent<Animator>().GetBool("Walk"))
			{
				TurnPhase = LocalManager.TurnPhases.Wait;
				manager.GlobalTurnPhase = LocalManager.TurnPhases.Wait;
				Moving = false;
				
				manager.MovingCharacter = null;
				
				manager.DeactivateAllTiles();
				
				manager.ActivateLocalCharacterColliders();
			}
		}

		if(TurnPhase == LocalManager.TurnPhases.Direction && manager.GlobalTurnPhase == LocalManager.TurnPhases.Direction)
		{
			Vector3 RelativeMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
			if(RelativeMousePosition.y > -RelativeMousePosition.x && RelativeMousePosition.y < RelativeMousePosition.x)
			{
				Direction = Directions.Right;
			}
			else if(RelativeMousePosition.y > -RelativeMousePosition.x && RelativeMousePosition.y > RelativeMousePosition.x)
			{
				Direction = Directions.Up;
			}
			else if(RelativeMousePosition.y < -RelativeMousePosition.x && RelativeMousePosition.y > RelativeMousePosition.x)
			{
				Direction = Directions.Left;
			}
			else if(RelativeMousePosition.y < -RelativeMousePosition.x && RelativeMousePosition.y < RelativeMousePosition.x)
			{
				Direction = Directions.Down;
			}
			List<LocalTile> AtackRange = ShowAndReturn_AttackRange();
			SetCharacterSpriteDirection(Direction);

			if(Input.GetMouseButtonDown(0))
			{
				FixDirection();
				isAlreadyAttacked = true;
			}

			if(Input.GetMouseButton(1))
			{
				transform.position = StartTile.transform.position + relativePosition_AboutTile(StartTile.Y);
				CurrentTile.CharacterOnTile = null;
				CurrentTile = StartTile;
				Direction = startDirection;
				Vector3 Scale = transform.localScale;
				Scale.x = startScaleX;
				transform.localScale = Scale;
				if(DamageText != null)
				{
					DamageText.gameObject.transform.localScale = Scale;
				}
				CurrentTile.CharacterOnTile = this;

				TurnPhase = LocalManager.TurnPhases.Move;
				manager.GlobalTurnPhase = LocalManager.TurnPhases.Move;
				manager.DeactivateLocalCharacterColliders();
				Moving = true;

				manager.MovingCharacter = this;

				manager.DeactivateAllTiles();

				List<LocalTile> MovableTiles = MovableRange();

				foreach(LocalTile MovableTile in MovableTiles)
				{
					if(MovableTile != null)
					{
						MovableTile.MyRenderer.sprite = MovableTile.MovableSprite;
						MovableTile.Movable = true;
					}
				}
			}
		}

		if(isAlreadyAttacked)
		{
			body.renderer.material.color = Color.gray;
			leftLeg.renderer.material.color = Color.gray;
			rightLeg.renderer.material.color = Color.gray;
		}

		else if(!isAlreadyAttacked)
		{
			body.renderer.material.color = Color.white;
			leftLeg.renderer.material.color = Color.white;
			rightLeg.renderer.material.color = Color.white;
		}
	}

	public void SetCharacterSpriteDirection(Directions dir)
	{
		Vector3 Scale = transform.localScale;
		
		if(dir == Directions.Right)
		{
			Scale.x = -1;
		}
		else if(dir == Directions.Left)
		{
			Scale.x = 1;
		}
		transform.localScale = Scale;
		if(DamageText != null)
		{
			DamageText.gameObject.transform.localScale = Scale;
		}
	}

	public void FixDirection()
	{
		List<LocalTile> AttackRange = manager.ShowAndReturn_AttackRange (this);

		TurnPhase = LocalManager.TurnPhases.Wait;
		manager.GlobalTurnPhase = LocalManager.TurnPhases.Wait;
		manager.RegisterAttackTile(AttackRange, manager.MovingCharacter);
		manager.RegisterHitObject (AttackRange, manager.MovingCharacter);
		manager.DeactivateAllTiles();
		manager.FinishedCharacters++;
		Activated = false;
		Moving = false;
		manager.MovingCharacter = null;
	}

	LocalTile RotatedRelativeTile(int X, int Y)
	{
		if(Direction == Directions.Right)
		{
			return manager.GetTileAt(CurrentTile.X+X, CurrentTile.Y+Y);
		}
		else if(Direction == Directions.Up)
		{
			return manager.GetTileAt(CurrentTile.X-Y, CurrentTile.Y+X);
		}
		else if(Direction == Directions.Left)
		{
			return manager.GetTileAt(CurrentTile.X-X, CurrentTile.Y-Y);
		}
		else
		{
			return manager.GetTileAt(CurrentTile.X+Y, CurrentTile.Y-X);
		}
	}

	Vector2 RotatedVector2(int X, int Y)
	{
		if(Direction == Directions.Right)
		{
			return new Vector2((float)(CurrentTile.X+X), (float)(CurrentTile.Y+Y));
		}
		else if(Direction == Directions.Up)
		{
			return new Vector2((float)(CurrentTile.X-Y), (float)(CurrentTile.Y+X));
		}
		else if(Direction == Directions.Left)
		{
			return new Vector2((float)(CurrentTile.X-X), (float)(CurrentTile.Y-Y));
		}
		else
		{
			return new Vector2((float)(CurrentTile.X+Y), (float)(CurrentTile.Y-X));
		}
	}

	public List<LocalTile> ShowAndReturn_AttackRange()
	{
		manager.DeactivateAllTiles();
		List<LocalTile> AttackRange = new List<LocalTile>();

		foreach(Vector2 AttackableTile in AttackableTiles())
		{
			AttackRange.Add (RotatedRelativeTile((int)(AttackableTile.x), (int)(AttackableTile.y)));
		}

		LocalManager.ChangeSpriteOfLocalTiles(AttackRange, CurrentTile.AttackableSprite);

		return AttackRange;
	}

	public List<Vector2> AttackableTiles()
	{
		return AttackableTileLists [CharacterIndex];
	}

	public void InstantiateEffect()
	{
		GameObject EffectPrefab = AttackEffects[CharacterIndex];

		if(CharacterIndex == 1)
		{
			Effect = Instantiate(EffectPrefab, manager.GetTilePositionAt(RotatedVector2(6, 0))-new Vector3(0, 0, 3), Quaternion.identity) as GameObject;
		}
		else if(CharacterIndex == 2)
		{
			Effect = Instantiate(EffectPrefab, (manager.GetTilePositionAt(RotatedVector2(3, 0))+manager.GetTilePositionAt(RotatedVector2(3, -1)))/2-new Vector3(0, 0, 3), Quaternion.identity) as GameObject;
		}
		else if(CharacterIndex == 6)
		{
			Effect = Instantiate(EffectPrefab, (manager.GetTilePositionAt(RotatedVector2(3, 0))+manager.GetTilePositionAt(RotatedVector2(4, 0)))/2-new Vector3(0, 0, 3), Quaternion.identity) as GameObject;
		}
		else
		{
			Effect = Instantiate(EffectPrefab, transform.position-new Vector3(0, 0, 2), Quaternion.identity) as GameObject;
			
			if(CharacterIndex == 0 || CharacterIndex == 5 || CharacterIndex == 7 || CharacterIndex >= 10)
			{
				Vector3 Rotation = Effect.transform.localRotation.eulerAngles;
				if(Direction == Directions.Right)
				{
					Rotation.z = 0;
				}
				else if(Direction == Directions.Up)
				{
					Rotation.z = 90;
				}
				else if(Direction == Directions.Left)
				{
					Rotation.z = 180;
				}
				else
				{
					Rotation.z = 270;
				}
				
				Effect.transform.localRotation = Quaternion.Euler(Rotation);
			}
		}
	}

	public Vector3 relativePosition_AboutTile(int tileY)
	{
		return new Vector3 (0, 0, tileY * Constants.diffPositionZ - 2);
	}

	public IEnumerator DoAttack(int index)
	{
		GetComponent<Animator> ().SetTrigger ("Attack");
		InstantiateEffect ();
		
		SoundEffect.PlayOneShot(AttackSounds[CharacterIndex], 1.0f);
		
		yield return new WaitForSeconds (Constants.timeToHitAni);
		
		foreach(LocalInteractableObject HitObject in HitObjects)
		{
			if(HitObject != null)
			{
				int chainCount = HitObject.CharactersForAttack.Count;
				
				if(HitObject.Buff)
				{
					HitObject.totalDamage += 1;
					HitObject.DamageText.text = HitObject.totalDamage+"";
					manager.PutHeal(this, (int) (MaxHP*chainMultiplierForHeal(chainCount)/10));
				}
				else
				{
					HitObject.totalDamage += (int) (Attack*chainMultiplierForAttack(chainCount));
					HitObject.DamageText.text = HitObject.CharactersForAttack.Count+" Chain\n"+HitObject.totalDamage;
					
					HitObject.GetComponent<Animator>().SetTrigger("Hit");
					SoundEffect.PlayOneShot(HitSound, 1.0F);
				}
			}
		}
		
		yield return new WaitForSeconds (1-Constants.timeToHitAni);
		
		Destroy (Effect.gameObject);
		
		if(manager.Attackers.Count > index+1)
		{
			StartCoroutine(manager.Attackers[index+1].DoAttack(index+1));
		}
		else
		{
			SoundEffect.PlayOneShot(HitSound, 1.0F);
			StartCoroutine(manager.ApplyDamageForAll());
		}
		
		LocalCutScene CurrentCutScene = GameObject.FindObjectOfType (typeof(LocalCutScene)) as LocalCutScene;
		if(CurrentCutScene != null)
		{
			Destroy(CurrentCutScene.gameObject);
		}
	}

	public float chainMultiplierForAttack(int chainCount)
	{
		if(chainCount == 1)
		{
			return 1.0f;
		}
		else if(chainCount == 2)
		{
			return 1.25f;
		}
		else if(chainCount == 3 || chainCount == 4)
		{
			return 1.5f;
		}
		else
		{
			return 0.0f;
		}
	}

	public float chainMultiplierForHeal(int chainCount)
	{
		if(chainCount == 1)
		{
			return 1.0f;
		}
		else if(chainCount == 2)
		{
			return 1.25f;
		}
		else if(chainCount == 3 || chainCount == 4)
		{
			return chainCount/2;
		}
		else
		{
			return 0;
		}
	}
}
