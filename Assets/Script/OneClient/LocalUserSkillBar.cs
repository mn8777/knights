﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class LocalUserSkillBar : MonoBehaviour
{
	public bool for1P;

	public Sprite[] PintosEndPoint = new Sprite[3];
	public Sprite[] HaskellEndPoint = new Sprite[3];

	public LocalManager manager;

	public SpriteRenderer SkillBarLv1;
	public SpriteRenderer SkillBarLv2;
	private SpriteRenderer SkillEnd;

	public Local_SelectionData selection;

	void Start()
	{
		SkillEnd = GetComponent<SpriteRenderer> ();
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
	}

	void Update()
	{
		int UserSkillCharge;
		if(for1P)
		{
			UserSkillCharge = manager.userSkillCharge1;
		}
		else
		{
			UserSkillCharge = manager.userSkillCharge2;
		}

		int SkillLevel = 0;
		
		if(UserSkillCharge <= Constants.SkillChargeMax/2)
		{
			if(UserSkillCharge == Constants.SkillChargeMax/2)
				SkillLevel = 1;

			Vector3 Lv1Scale = SkillBarLv1.transform.localScale;
			Lv1Scale.x = 2*UserSkillCharge*Constants.ScaleOfSkillBar/Constants.SkillChargeMax;
			SkillBarLv1.transform.localScale = Lv1Scale;
			
			Vector3 Lv2Scale = SkillBarLv2.transform.localScale;
			Lv2Scale.x = 0;
			SkillBarLv2.transform.localScale = Lv2Scale;
		}
		else
		{
			SkillLevel = 1;

			if(UserSkillCharge == Constants.SkillChargeMax)
				SkillLevel = 2;

			Vector3 Lv1Scale = SkillBarLv1.transform.localScale;
			Lv1Scale.x = Constants.ScaleOfSkillBar;
			SkillBarLv1.transform.localScale = Lv1Scale;
			
			Vector3 Lv2Scale = SkillBarLv2.transform.localScale;
			Lv2Scale.x = 2*(UserSkillCharge-Constants.SkillChargeMax/2)*Constants.ScaleOfSkillBar/Constants.SkillChargeMax;
			SkillBarLv2.transform.localScale = Lv2Scale;
		}

		SkillEnd.sprite = GetSprites (GetTeam (for1P)) [SkillLevel];
	}

	Teams GetTeam(bool is1P)
	{
		if (is1P)
			return selection.team1;
		else
			return selection.team2;
	}

	Sprite[] GetSprites(Teams team)
	{
		if (team == Teams.Pintos)
			return PintosEndPoint;
		else
			return HaskellEndPoint;
	}
}
