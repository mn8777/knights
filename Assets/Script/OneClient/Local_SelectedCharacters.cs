﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class Local_SelectedCharacters : MonoBehaviour
{
	public bool[] clickedSelectedCharacter = new bool[Constants.ofTeamMembers];
	public int[] slot = new int[Constants.ofTeamMembers];
	public Local_SelectionData selection;
	
	void Start () 
	{
		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;
		for(int i = 0; i < Constants.ofTeamMembers ; i ++)
		{
			slot[i] = Constants.ofCharactersInEachTeam; //anybignumber
			clickedSelectedCharacter[i]=false;
		}
	}

	void Update () 
	{
		if(Local_ReadyCounter.clicked)
		{
			for(int i = 0; i < Constants.ofTeamMembers ; i ++)
			{
				if(slot[i]==Constants.ofCharactersInEachTeam)
				{
					slot[i]=selection.currentSelection;
					GetCharacter(selection.currentPlayer, i);
					Local_ReadyCounter.clicked=false;
					Local_ReadyCounter.readyCount++;
					break;
				}
			}
		}
		
		for(int i = 0; i < Constants.ofTeamMembers ; i ++)
		{
			if(clickedSelectedCharacter[i])
			{
				CancelSelection (selection.currentPlayer, i);
				slot[i]=Constants.ofCharactersInEachTeam;
				Local_ReadyCounter.readyCount--;
				clickedSelectedCharacter[i]=false;
				break;
			}
		}
	}

	void GetCharacter(int player, int i)
	{
		if(player == 1)
		{
			selection.character1[i] = slot[i];
		}
		else if(player == 2)
		{
			selection.character2[i] = slot[i];
		}
	}

	void CancelSelection(int player, int i)
	{
		if(player == 1)
		{
			selection.character1[i] = Constants.ofCharactersInEachTeam;
		}
		else if(player == 2)
		{
			selection.character2[i] = Constants.ofCharactersInEachTeam;
		}
	}
}
