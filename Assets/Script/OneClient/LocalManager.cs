﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;
using CommonConstants;

//FIXME:Please Fix the FIXME!!!!!!
//InGame UI, FaceMouseOverUI, CharacterInfoSheet, TileRandom, ResetVariables, CharacterSelect
//수치 건드리는 함수 전반적으로 다시 다 검토할 필요 있음. GetSkillChargeOfPlayer를 감소시킨다고 UserSKillCharge n이 감소하진 않음.

public class LocalManager : MonoBehaviour 
{
	public int userSkillCharge1;
	public int userSkillCharge2;

	public List<Vector2> MoveLevel1;
	public List<Vector2> MoveLevel2;
	public List<Vector2> MoveLevel3;
	public List<Vector2> MoveLevel4;
	public List<Vector2> MoveLevel5;
	public List<Vector2> MoveLevel6;
	public List<Vector2> MoveLevel7;
	public List<Vector2> MoveLevel8;
	public List<Vector2> MoveLevel9;
	public List<Vector2> MoveLevel10;
	
	public List<List<Vector2>> MoveLevels = new List<List<Vector2>>();

	public LocalCharacter CharacterPrefab;
	public LocalCharacter MovingCharacter;

	public LocalInteractableObject MostChainedObject;

	public LocalInformationText infoText1;
	public LocalInformationText infoText2;
	public LocalCharacterInfoSheetInGameUI infoSheet;

	public bool AbleToTurnEnd;
	public int FinishedCharacters;
	public List<LocalCharacter> Characters;

	public int Turn;
	public List<LocalTile> Tiles;
	public LocalTile TilePf;
	private List<LocalTile> TilesForPlayer1 = new List<LocalTile>();
	private List<LocalTile> TilesForPlayer2 = new List<LocalTile>();
	private List<LocalTile> TilesForBuffSpawn = new List<LocalTile>();

	public LocalNoticeMessage MessagePrefab;
	public LocalNoticeMessage StaticMessagePrefab;
	public GameObject Rock;
	public LocalBuff BuffPrefab;
	public static LocalBuff StaticBuffPrefab;
	public Local_SelectionData selection;

	public LocalTurnEffect[] TurnEffects;
	public static LocalTurnEffect[] StaticTurnEffects;

	public string[] Names;
	public static string[] staticNames;

	public enum TurnPhases
	{
		Wait,
		Move,
		Direction,
	}
	public TurnPhases GlobalTurnPhase;

	public LocalCutScene CutScene2;
	public LocalCutScene CutScene3;
	public LocalCutScene CutScene4;

	public List<LocalCharacter> Attackers;

	public LocalInGameUI[] InGameUIs;

	public BGMManager manager_b;

	int BuffSpawnTime;
	bool alreadyFinishedTurn;
	bool alreadyEndedGame;

	public GameObject healEffect;

	void Start()
	{
		Characters.Clear ();
		Tiles.Clear ();
		TilesForPlayer1.Clear ();
		TilesForPlayer2.Clear ();
		TilesForBuffSpawn.Clear ();

		DontDestroyOnLoad (gameObject);
		
		//9단계의 이동 범위를 불러올 수 있게 세팅함
		MoveLevels.Add (MoveLevel1);
		MoveLevels.Add (MoveLevel2);
		MoveLevels.Add (MoveLevel3);
		MoveLevels.Add (MoveLevel4);
		MoveLevels.Add (MoveLevel5);
		MoveLevels.Add (MoveLevel6);
		MoveLevels.Add (MoveLevel7);
		MoveLevels.Add (MoveLevel8);
		MoveLevels.Add (MoveLevel9);
		MoveLevels.Add (MoveLevel10);

		StaticMessagePrefab = MessagePrefab;
		StaticTurnEffects = TurnEffects;
		StaticBuffPrefab = BuffPrefab;
		staticNames = Names;

		selection = GameObject.FindObjectOfType (typeof(Local_SelectionData)) as Local_SelectionData;

		userSkillCharge1 = 0;
		userSkillCharge2 = 0;
		FinishedCharacters = 0;
		Turn = 1;

		SpawnTiles ();
		SpawnKnights();
		SpawnRocks();
		
		LocalNoticeMessage Message = Instantiate(MessagePrefab, new Vector3(Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as LocalNoticeMessage;
		Message.GetComponent<SpriteRenderer>().sprite = Message.GameStart;
		
		BuffSpawnTime = 2;

		foreach(LocalTurnEffect turnEffect in TurnEffects)
		{
			turnEffect.DoSetting();
		}

		alreadyFinishedTurn = false;
		alreadyEndedGame = false;

		manager_b = GameObject.FindObjectOfType (typeof(BGMManager)) as BGMManager;
		if(manager_b != null)
		{
			manager_b.ChangeBgmAtBattle ();
		}
	}

	void Update()
	{

		if(FinishedCharacters == aliveCountForPlayer(Turn) && alreadyFinishedTurn == false && alreadyEndedGame == false)
		{
			FinishedCharacters = 0;
			alreadyFinishedTurn = true;
			ApplyAttacks();
		}

		foreach(LocalCharacter Knight in Characters)
		{
			if(Knight != null)
			{
				if(Knight.validCharacter(Turn))
					Knight.ring.sr.sprite = Knight.ring.Rings[1];
				else
					Knight.ring.sr.sprite = Knight.ring.Rings[0];
			}
		}
	}

	public int GetSkillChargeOfPlayer(int player)
	{
		if (player == 1)
			return userSkillCharge1;
		else
			return userSkillCharge2;
	}

	public IEnumerator ApplyDamageForAll ()
	{
		foreach(LocalCharacter Knight in Characters)
		{
			if(!Knight.validCharacter(Turn) && Knight.totalDamage != 0)
			{
				ApplyDamage(Knight, Knight.totalDamage);
			}
		}
		
		LocalBuff BuffObject = GameObject.FindObjectOfType (typeof(LocalBuff)) as LocalBuff;
		if(BuffObject != null && BuffObject.CharactersForAttack.Count != 0)
		{
			BuffObject.HP -= BuffObject.totalDamage;
			
			if(BuffObject.HP <= 0)
			{
				Destroy (BuffObject.gameObject);

				foreach(LocalTile tile in Tiles)
				{
					if(tile != null)
					{
						tile.BuffOnTile = null;
					}
				}
				
				BuffSpawnTime = Constants.BuffSpawnDuration;
			}
		}
		
		yield return new WaitForEndOfFrame ();

		Debug.Log ("Successful Applying DamageForAll");
		EndTurn();
	}

	public void ApplyDamage(LocalCharacter Knight, int Damage)
	{
		if(Knight != null)
		{
			Knight.HP -= Damage;
			Knight.GetComponent<Animator>().SetTrigger("Hit");
			Debug.Log("Successful Damage Apply");
			
			DestroyDyingKnight(Knight);
			SkillCharge(Damage);
		}
	}

	public void ApplyHaskellBuff(LocalCharacter Knight, int Power)
	{
		Knight.Attack = Constants.TickAttack * (Knight.IndexAttack [Knight.CharacterIndex] + Power) + Constants.StandardAttack;
		Knight.Move = Knight.IndexMove [Knight.CharacterIndex] + Power;
	}

	public static void ChangeSpriteOfLocalTiles(List<LocalTile> TileList, Sprite NewSprite)
	{
		foreach(LocalTile tile in TileList)
		{
			if(tile != null)
			{
				tile.MyRenderer.sprite = NewSprite;
			}
		}
	}

	public void ActivateLocalCharacterColliders()
	{
		foreach(LocalCharacter Knight in Characters)
		{
			if(Knight != null)
			{
				Knight.GetComponent<BoxCollider2D>().enabled = true;
			}
		}
	}

	public void DeactivateLocalCharacterColliders()
	{
		foreach(LocalCharacter Knight in Characters)
		{
			if(Knight != null)
			{
				Knight.GetComponent<BoxCollider2D>().enabled = false;
			}
		}
	}

	void ChangeTurn()
	{
		Turn = 3 - Turn;
	}

	public void ShowTurnMessage()
	{
		if(alreadyEndedGame == false)
		{
			if (Turn == 1) 
			{
				if(aliveCountForPlayer(1) != 0)
				{
					Debug.Log(aliveCountForPlayer(1).ToString());
					if (selection.team1 == Teams.Pintos) 
					{
						LocalNoticeMessage Message = Instantiate (MessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as LocalNoticeMessage;
						Message.GetComponent<SpriteRenderer> ().sprite = Message.Pintos1P;
					}
					else 
					{
						LocalNoticeMessage Message = Instantiate (MessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as LocalNoticeMessage;
						Message.GetComponent<SpriteRenderer> ().sprite = Message.Haskell1P;
					}
				}
			}
			else 
			{	
				if(aliveCountForPlayer(2) != 0)
				{
					Debug.Log(aliveCountForPlayer(2).ToString());
					if (selection.team2 == Teams.Pintos) 
					{
						LocalNoticeMessage Message = Instantiate (MessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as LocalNoticeMessage;
						Message.GetComponent<SpriteRenderer> ().sprite = Message.Pintos2P;
					}
					else 
					{
						LocalNoticeMessage Message = Instantiate (MessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as LocalNoticeMessage;
						Message.GetComponent<SpriteRenderer> ().sprite = Message.Haskell2P;
					}
				}
			}
		}
	}
	
	void SpawnKnights()
	{
		for (int i = 0; i < Constants.ofTeamMembers; i++)
		{
			int SpawnedTile = UnityEngine.Random.Range (0, TilesForPlayer1.Count);
			LocalCharacter CharacterForSetting = Instantiate (CharacterPrefab, TilesForPlayer1 [SpawnedTile].transform.position + new Vector3 (0, 1, -1.5f), Quaternion.identity) as LocalCharacter;
			CharacterForSetting.CharacterIndex = selection.character1 [i]+return10IfHaskell(selection.team1);
			CharacterForSetting.name = Names[CharacterForSetting.CharacterIndex];
			CharacterForSetting.Activated = true;
			CharacterForSetting.CurrentTile = TilesForPlayer1 [SpawnedTile];
			CharacterForSetting.StartTile = CharacterForSetting.CurrentTile;
			CharacterForSetting.manager = this;
			CharacterForSetting.Direction = Directions.Right;
			CharacterForSetting.startDirection = CharacterForSetting.Direction;
			CharacterForSetting.startScaleX = CharacterForSetting.transform.localScale.x;
			CharacterForSetting.GameCharacterIndex = i;
			Characters.Add (CharacterForSetting);
			CharacterForSetting.body.ParentCharacter = CharacterForSetting;
			CharacterForSetting.leftLeg.ParentCharacter = CharacterForSetting;
			CharacterForSetting.rightLeg.ParentCharacter = CharacterForSetting;
			CharacterForSetting.ring.sr.sprite = CharacterForSetting.ring.Rings[0];
			CharacterForSetting.wing.sr.sprite = CharacterForSetting.wing.Wings[0];
			TilesForPlayer1 [SpawnedTile].CharacterOnTile = CharacterForSetting;
			TilesForPlayer1.RemoveAt (SpawnedTile);
		}
			
		for (int i = 0; i < Constants.ofTeamMembers; i++) 
		{
			int SpawnedTile = UnityEngine.Random.Range (0, TilesForPlayer2.Count);
			LocalCharacter CharacterForSetting = Instantiate (CharacterPrefab, TilesForPlayer2 [SpawnedTile].transform.position + new Vector3 (0, 1, -1.5f), Quaternion.identity) as LocalCharacter;
			CharacterForSetting.CharacterIndex = selection.character2 [i]+return10IfHaskell(selection.team2);
			CharacterForSetting.name = Names[CharacterForSetting.CharacterIndex];
			CharacterForSetting.CurrentTile = TilesForPlayer2 [SpawnedTile];
			CharacterForSetting.StartTile = CharacterForSetting.CurrentTile;
			CharacterForSetting.manager = this;
			CharacterForSetting.Direction = Directions.Left;
			CharacterForSetting.startDirection = CharacterForSetting.Direction;
			Vector3 Scale = CharacterForSetting.transform.localScale;
			Scale.x = -0.5f;
			CharacterForSetting.startScaleX = CharacterForSetting.transform.localScale.x;
			CharacterForSetting.transform.localScale = Scale;
			CharacterForSetting.SetCharacterSpriteDirection(CharacterForSetting.Direction);
			CharacterForSetting.GameCharacterIndex = i + Constants.ofTeamMembers;
			Characters.Add (CharacterForSetting);
			CharacterForSetting.body.ParentCharacter = CharacterForSetting;
			CharacterForSetting.leftLeg.ParentCharacter = CharacterForSetting;
			CharacterForSetting.rightLeg.ParentCharacter = CharacterForSetting;
			CharacterForSetting.ring.sr.sprite = CharacterForSetting.ring.Rings[0];
			CharacterForSetting.wing.sr.sprite = CharacterForSetting.wing.Wings[0];
			TilesForPlayer2 [SpawnedTile].CharacterOnTile = CharacterForSetting;
			TilesForPlayer2.RemoveAt (SpawnedTile);
		}

		foreach (LocalInGameUI UI in InGameUIs)
		{
			UI.SetCharacterNames();
		}

		infoText1.SetCharacterNames ();
		infoText2.SetCharacterNames ();
		infoSheet.SetCharacterNames ();
	}

	void SpawnRocks()
	{
		for(int i = 0; i < Constants.RockCount; i++)
		{
			List<LocalTile> AvailableTiles = new List<LocalTile> ();
			foreach(LocalTile tile in Tiles)
			{
				if(tile.CharacterOnTile == null && tile.Occupied == false)
				{
					AvailableTiles.Add (tile);
				}
			}
			
			LocalTile SpawnedTile = AvailableTiles[UnityEngine.Random.Range(0, AvailableTiles.Count)];
			Instantiate(Rock, SpawnedTile.transform.position - new Vector3(0, 0, 1), Quaternion.identity);
			SpawnedTile.Occupied = true;
		}
	}

	void SpawnLocalBuff()
	{
		List<LocalTile> AvailableTiles = new List<LocalTile> ();
		foreach(LocalTile tile in TilesForBuffSpawn)
		{
			if(tile.CharacterOnTile == null && tile.Occupied == false)
			{
				AvailableTiles.Add (tile);
			}
		}
		
		LocalTile SpawnedTile = AvailableTiles [UnityEngine.Random.Range (0, AvailableTiles.Count)];
		LocalBuff BuffInstance = Instantiate (BuffPrefab, SpawnedTile.transform.position - new Vector3 (0, 0, 1), Quaternion.identity) as LocalBuff;
		SpawnedTile.BuffOnTile = BuffInstance;
	}

	public void DeactivateAllTiles()
	{
		foreach(LocalTile tile in Tiles)
		{
			tile.MyRenderer.sprite = tile.DefaultSprite;
			tile.Movable = false;
		}
	}

	void DestroyDyingKnight(LocalCharacter Knight)
	{
		if(Knight.HP <= 0)
		{
			Knight.HP = 0;
			Knight.CurrentTile.CharacterOnTile = null;
			Knight.gameObject.AddComponent<SelfDestroy>().timer = 1.2f;
			Animator knightAnim = Knight.GetComponent<Animator>();
			knightAnim.SetTrigger ("Die");
		}
	}

	void SkillCharge(int Damage)
	{
		if(Turn == 1)
		{
			userSkillCharge2 += Damage/Constants.DamagePerSkillCharge;
			
			if(userSkillCharge2 > Constants.SkillChargeMax)
			{
				userSkillCharge2 = Constants.SkillChargeMax;
			}
		}
		else
		{
			userSkillCharge1 += Damage/Constants.DamagePerSkillCharge;
			
			if(userSkillCharge1 > Constants.SkillChargeMax)
			{
				userSkillCharge1 = Constants.SkillChargeMax;
			}
		}
	}
	
	public void PutHeal(LocalCharacter Knight, int Heal)
	{
		Knight.HP += Heal;
		GameObject HealInstance = Instantiate (healEffect, Knight.gameObject.transform.position, Quaternion.identity) as GameObject;
		HealInstance.AddComponent<SelfDestroy> ();
		
		if(Knight.HP > Knight.MaxHP)
		{
			Knight.HP = Knight.MaxHP;
		}
	}
	
	public void EndTurn()
	{
		MovingCharacter = null;
		AbleToTurnEnd = false;
		alreadyFinishedTurn = false;

		foreach(LocalCharacter Knight in Characters)
		{
			if(Knight != null)
			{
//				Knight.GetComponent<SpriteRenderer>().color = Color.white;
				Knight.Activated = true;
				Knight.isAlreadyAttacked = false;
				Knight.GetComponent<Animator>().SetBool("Done",false);
				
				if(Knight.ObjectBuffDuration > 0)
				{
					Knight.ObjectBuffDuration -= 1;
				}

				Knight.wing.sr.sprite = Knight.wing.Wings[0];
				
				Knight.Attack = Constants.TickAttack * Knight.IndexAttack [Knight.CharacterIndex] + Constants.StandardAttack;
				Knight.Move = Knight.IndexMove [Knight.CharacterIndex];
				Knight.totalDamage = 0;
				if(Knight.DamageText != null)
				{
					Knight.DamageText.text = "";
				}
				Knight.HitObjects.Clear();
				Knight.CharactersForAttack.Clear();
			}
		}
		
		DeactivateAllTiles ();

		if(aliveCountForPlayer(1) == 0 || aliveCountForPlayer(2) == 0)
		{
			alreadyEndedGame = true;
			LocalNoticeMessage Message = Instantiate (MessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as LocalNoticeMessage;
			Message.GetComponent<SpriteRenderer> ().sprite = Message.GameEnd;
		}
		else if(aliveCountForPlayer(1) != 0 && aliveCountForPlayer(2) != 0)
		{
			ChangeTurn ();
			ShowTurnMessage ();
		}

		BuffSpawnTime -= 1;
		
		if(buffExistence())
		{
			LocalBuff BuffObject = GameObject.FindObjectOfType (typeof(LocalBuff)) as LocalBuff;
			BuffObject.CharactersForAttack.Clear();
			BuffObject.totalDamage = 0;
			if(BuffObject.DamageText != null)
			{
				BuffObject.DamageText.text = "";
			}
		}
		else if(BuffSpawnTime <= 0)
		{
			SpawnLocalBuff();
		}

		foreach(LocalTurnEffect turnEffect in TurnEffects)
		{
			turnEffect.DoSetting();
		}

		Attackers.Clear ();
	}

	public int aliveCountForPlayer (int player)
	{
		int result = 0;
		foreach(LocalCharacter Knight in Characters)
		{
			if(Knight != null && Knight.HP != 0 && Knight.validCharacter(player))
				result++;
		}
		
		return result;
	}
	
	bool buffExistence()
	{
		bool Result = false;
		
		foreach(LocalTile tile in Tiles)
		{
			if(tile.BuffOnTile != null)
			{
				Result = true;
			}
		}
		
		return Result;
	}

	public LocalTile GetTileAt(int X, int Y)
	{
		if(X >= 1 && X <= Constants.MapSize && Y >= 1 && Y<= Constants.MapSize)
		{
			return Tiles[Constants.MapSize*(Y-1)+X-1];
		}
		else
		{
			return null;
		}
	}

	public bool isCharacter1P(LocalCharacter Knight)
	{
		if(Knight.GameCharacterIndex < Constants.ofTeamMembers)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	int return10IfHaskell(Teams team)
	{
		if(team == Teams.Pintos)
		{
			return 0;
		}
		else
		{
			return Constants.ofCharactersInEachTeam;
		}
	}

	void FindMostChainedCharacter()
	{
		MostChainedObject = null;
		foreach(LocalCharacter Knight in Characters)
		{
			if(Knight != null)
			{
				if(!Knight.validCharacter(Turn))
				{
					if(MostChainedObject == null)
					{
						MostChainedObject = Knight;
					}
					else if(Knight.CharactersForAttack.Count > MostChainedObject.CharactersForAttack.Count)
					{
						MostChainedObject = Knight;
					}
				}
			}
		}
	}

	public void ApplyAttacks ()
	{
		Debug.Log ("Successful Applying Attack for All");
		FindMostChainedCharacter ();

		LocalBuff BuffObject = GameObject.FindObjectOfType (typeof(LocalBuff)) as LocalBuff;

		if(MostChainedObject != null)
		{
			if(BuffObject != null && MostChainedObject.CharactersForAttack.Count < BuffObject.CharactersForAttack.Count)
			{
				MostChainedObject = BuffObject;
			}
		}
		else if (BuffObject != null)
		{
			MostChainedObject = BuffObject;
		}

		if(MostChainedObject != null)
		{
			foreach(LocalCharacter Attacker in MostChainedObject.CharactersForAttack)
			{
				Attackers.Add(Attacker);
			}
		}
		
		foreach(LocalCharacter Knight in Characters)
		{
			bool isAlreadyRegistered = false;
			
			foreach(LocalCharacter Attacker in Attackers)
			{
				if(Knight == Attacker)
				{
					isAlreadyRegistered = true;
				}
			}
			
			if(!isAlreadyRegistered && Knight.HitObjects.Count != 0)
			{
				Attackers.Add (Knight);
			}
		}
		
		if(Attackers.Count != 0)
		{
			if(MostChainedObject != null && MostChainedObject.CharactersForAttack.Count == 1)
			{
				StartCoroutine(Attackers[0].DoAttack(0));
			}
			else
			{
				ShowCutScene();
			}
		}
		else
		{
			EndTurn();
		}
	}

	void ShowCutScene()
	{
		LocalCutScene CutSceneForSetting = null;
		if(MostChainedObject.CharactersForAttack.Count == 2)
		{
			CutSceneForSetting = Instantiate(CutScene2) as LocalCutScene;
		}
		else if(MostChainedObject.CharactersForAttack.Count == 3)
		{
			CutSceneForSetting = Instantiate(CutScene3) as LocalCutScene;
		}
		else if(MostChainedObject.CharactersForAttack.Count == 4)
		{
			CutSceneForSetting = Instantiate(CutScene4) as LocalCutScene;
		}

		CutSceneForSetting.MostChainedObject = MostChainedObject;
		if(Turn == 1)
		{
			CutSceneForSetting.Team = selection.team1;
		}
		else
		{
			CutSceneForSetting.Team = selection.team2;
		}
	}

	void SpawnTiles ()
	{
		for (int y = 1; y <= Constants.MapSize; y++) 
		{
			for (int x = 1; x <= Constants.MapSize; x++) 
			{
				LocalTile TileForAddList = Instantiate (TilePf, new Vector3 (x * Constants.TileDistanceX, y * Constants.TileDistanceY -2, 2), Quaternion.identity) as LocalTile;
				TileForAddList.X = x;
				TileForAddList.Y = y;
				TileForAddList.manager = this;
				TileForAddList.name = "Tile(" + TileForAddList.X + "," + TileForAddList.Y + ")";
				TileForAddList.transform.parent = transform;
				Tiles.Add (TileForAddList);
				
				if (x == Constants.xPosition1P && y >= Constants.MinCharacterPositionY && y <= Constants.MaxCharacterPositionY) 
				{
					TilesForPlayer1.Add (TileForAddList);
				}
				else if (x == Constants.xPosition2P && y >= Constants.MinCharacterPositionY && y <= Constants.MaxCharacterPositionY) 
				{
					TilesForPlayer2.Add (TileForAddList);
				}
				else if(x >= Constants.MinBuffPositionX && x <= Constants.MaxBuffPositionX)
				{
					TilesForBuffSpawn.Add (TileForAddList);
				}
			}
		}
	}
	
	public void RegisterHitObject(List<LocalTile> TileList, LocalCharacter Attacker)
	{
		foreach(LocalTile tile in TileList)
		{
			if(tile != null)
			{
				if(tile.BuffOnTile != null)
				{
					tile.BuffOnTile.CharactersForAttack.Add (Attacker);
					Attacker.HitObjects.Add (tile.BuffOnTile);
				}
				else if(tile.CharacterOnTile != null && !tile.CharacterOnTile.validCharacter(Turn))
				{
					tile.CharacterOnTile.CharactersForAttack.Add (Attacker);
					Attacker.HitObjects.Add (tile.CharacterOnTile);
				}
			}
		}
	}

	public void RegisterAttackTile(List<LocalTile> TileList, LocalCharacter Attacker)
	{
		foreach(LocalTile tile in TileList)
		{
			if(tile != null)
			{
				if(tile.CharacterOnTile != null && !tile.CharacterOnTile.validCharacter(Turn))
				{
					if(tile.CharactersForAttack.Count == 1)
					{
						tile.CharacterOnTile.GetComponent<SpriteRenderer>().color = Color.yellow;
					}
					else if(tile.CharactersForAttack.Count == 2)
					{
						tile.CharacterOnTile.GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0);
					}
					else if(tile.CharactersForAttack.Count >= 3)
					{
						tile.CharacterOnTile.GetComponent<SpriteRenderer>().color = Color.red;
					}
				}
			}
		}
	}

	public List<LocalTile> ShowAndReturn_AttackRange(LocalCharacter Attacker)
	{
		DeactivateAllTiles();
		List<LocalTile> AttackRange = new List<LocalTile>();
		List<Vector2> AttackableTiles = Attacker.AttackableTiles();
		LocalTile CurrentTile = Attacker.CurrentTile;
		
		foreach(Vector2 AttackableTile in AttackableTiles)
		{
			if(Attacker.Direction == Directions.Right)
			{
				AttackRange.Add (GetTileAt(CurrentTile.X+(int)(AttackableTile.x), CurrentTile.Y+(int)(AttackableTile.y)));
			}
			else if(Attacker.Direction == Directions.Up)
			{
				AttackRange.Add (GetTileAt(CurrentTile.X-(int)(AttackableTile.y), CurrentTile.Y+(int)(AttackableTile.x)));
			}
			else if(Attacker.Direction == Directions.Left)
			{
				AttackRange.Add (GetTileAt(CurrentTile.X-(int)(AttackableTile.x), CurrentTile.Y-(int)(AttackableTile.y)));
			}
			else if(Attacker.Direction == Directions.Down)
			{
				AttackRange.Add (GetTileAt(CurrentTile.X+(int)(AttackableTile.y), CurrentTile.Y-(int)(AttackableTile.x)));
			}
		}
		
		ChangeSpriteOfLocalTiles(AttackRange, Attacker.CurrentTile.AttackableSprite);
		
		return AttackRange;
	}
	
	/*
	public void MoveAndFix_Character_BeforeMoveAni(int CharacterIndex, int TileX, int TileY, Directions Direction)
	{
		LocalCharacter Knight = Characters [CharacterIndex];
		Knight.CurrentTile.CharacterOnTile = null;
		Knight.CurrentTile = GetTileAt (TileX, TileY);
		
		iTween.MoveTo(Knight.gameObject, iTween.Hash("position", Knight.CurrentTile.transform.position + new Vector3(0, 1, -1), "Speed", Constants.MoveAniSpeed, "oncompletetarget", this.gameObject, "oncomplete", "MoveAndFix_Character_AfterMoveAni", "oncompleteparams", Knight));
		
		Knight.CurrentTile.CharacterOnTile = Knight;
		Knight.Direction = Direction;
		
		if(Direction == Directions.Right)
		{
			Vector3 Scale = Knight.transform.localScale;
			Scale.x = 0.5f;
			Knight.transform.localScale = Scale;
		}
		else if(Direction == Directions.Left)
		{
			Vector3 Scale = Knight.transform.localScale;
			Scale.x = -0.5f;
			Knight.transform.localScale = Scale;
		}
		DeactivateAllTiles ();
	}

//	MoveInfo [0] = GameCharacterIndex;
//	MoveInfo [1] = CurrentTile.X;
//	MoveInfo [2] = CurrentTile.Y;
//	MoveInfo [3] = Manager.DirectionToInteger (Direction);
	
	public void MoveAndFix_Character_AfterMoveAni(LocalCharacter Knight)
	{
		List<LocalTile> AttackRange = ShowAndReturn_AttackRange (Knight);
		
		RegisterAttackTile (AttackRange, Knight);
		DeactivateAllTiles ();
		Knight.Activated = false;
		
		FinishedCharacters += 1;
		
		int AliveCharacterNumbers = 0;
		if(Turn == 2)
		{
			for(int i = 0; i<Constants.ofTeamMembers; i++)
			{
				if(Characters[i] != null)
				{
					AliveCharacterNumbers += 1;
				}
			}
		}
		else
		{
			for(int i = Constants.ofTeamMembers; i<Constants.ofTeamMembers*2; i++)
			{
				if(Characters[i] != null)
				{
					AliveCharacterNumbers += 1;
				}
			}
		}
	}
	*/

	public Vector3 GetTilePositionAt(Vector2 inputVector)
	{
		return new Vector3 (3.3f * inputVector.x, 3.3f * inputVector.y - 2, Tiles [0].transform.position.z);
	}

	public bool isPlayer1P (int player)
	{
		if( player == 1 )
			return true;
		else
			return false;
	}

	public Teams currentTeam (int player)
	{
		if (player == 1)
			return selection.team1;
		else
			return selection.team2;
	}

	public int ReturnCharacterCount_IfPlayer2(bool player)
	{
		if (player == true)
			return 0;
		else
			return Constants.ofTeamMembers;
	}
	
	public void TileClear()
	{
		foreach(LocalTile tile in Tiles)
		{
			Destroy(tile);
		}
	}
}