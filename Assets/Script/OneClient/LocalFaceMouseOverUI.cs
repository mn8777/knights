﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class LocalFaceMouseOverUI : MonoBehaviour
{
	public int faceNumber;
	
	void OnMouseOver() {
		LocalCharacterInfoSheetInGameUI.viewedInfoSheet = faceNumber;
	}
	void OnMouseExit() {
		LocalCharacterInfoSheetInGameUI.viewedInfoSheet = Constants.ofCharactersInEachTeam*2;
	}
}
