using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class Local_SwitchTeam : MonoBehaviour
{
	
	public Sprite pintos;
	public Sprite haskel;
	public Local_SelectionData selection;
	
	private SpriteRenderer sr;

	void Start ()
	{
		selection = GameObject.FindObjectOfType(typeof(Local_SelectionData)) as Local_SelectionData;
		sr = GetComponent<SpriteRenderer>();
		
		if(selection.currentTeam==Teams.Pintos)
		{
			sr.sprite=pintos;
		}
		else if(selection.currentTeam==Teams.Haskell)
		{
			sr.sprite=haskel;
		}
	}
}