﻿using UnityEngine;
using System.Collections;

public class CreditsManager : MonoBehaviour {

	public GameObject camera;

	int currentPages = 1;

	void OnMouseDown()
	{
		if (currentPages == 1)
		{
			currentPages = 2;
			camera.transform.position += Vector3.right * 20;
		}
		else
		{
			currentPages = 1;
			BGMManager bgmPlayer = FindObjectOfType(typeof(BGMManager)) as BGMManager;
			if (bgmPlayer != null)
			{
				Destroy (bgmPlayer.gameObject);
			}
			Application.LoadLevel("TitleScene");
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
