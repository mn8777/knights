﻿using UnityEngine;
using System.Collections;

public class AllButtonManagerAtTitle : MonoBehaviour {

	public static AllButtonManagerAtTitle AllButtonManagerInstance = null;

	public GameObject startButton;
	public GameObject startLocalButton;
	public GameObject creditsButton;
	public GameObject endButton;

	public void AllButtonColliderDeactive()
	{
		startButton.collider2D.enabled = false;
		startLocalButton.collider2D.enabled = false;
		creditsButton.collider2D.enabled = false;
		endButton.collider2D.enabled = false;
	}

	void Awake()
	{
		AllButtonManagerInstance = this;
	}

	public static AllButtonManagerAtTitle GetAllButtonManagerInstance()
	{
		return AllButtonManagerInstance;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
