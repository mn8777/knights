﻿using UnityEngine;
using System.Collections;

public class BGMManager : MonoBehaviour {

	private AudioSource bgmPlayer;

	public AudioClip nonBattleBgm;
	public AudioClip battleBgm;

	void Awake()
	{
		DontDestroyOnLoad(this);
		bgmPlayer = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () {
		bgmPlayer.loop = true;
		bgmPlayer.clip = nonBattleBgm;
		bgmPlayer.Play();
	}

	public void ChangeBgmAtBattle()
	{
		bgmPlayer.Stop();
		bgmPlayer.clip = battleBgm;
		bgmPlayer.Play();
	}

	// Update is called once per frame
	void Update () {

	}
}
