﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour {

	public Sprite deactivatedButton;
	public Sprite activatedButton;

	public AudioClip mouseOverSE;
	public AudioClip mouseClickSE;

	private SpriteRenderer sr;
	private AudioSource se;

	void Awake()
	{
		se = GetComponent<AudioSource>();
	}

	void Start()
	{
		sr = GetComponent<SpriteRenderer>();
	}

	void OnMouseDown ()
	{
		StartCoroutine("MouseClick");
	}

	void Deactivate(){
		sr.sprite = deactivatedButton;
	}

	void Activate(){
		sr.sprite = activatedButton;
	}

	IEnumerator MouseClick()
	{
		AllButtonManagerAtTitle.GetAllButtonManagerInstance().AllButtonColliderDeactive();
		se.PlayOneShot(mouseClickSE, 1);
		yield return new WaitForSeconds(1);
		Application.Quit();
	}

	void OnMouseEnter ()
	{
		se.PlayOneShot(mouseOverSE, 1);
		Activate();
	}

	void OnMouseExit()
	{
		Deactivate();
	}
}
