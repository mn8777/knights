﻿using UnityEngine;
using System.Collections;

public class StartButton : MonoBehaviour 
{
	public Sprite deactivatedButton;
	public Sprite activatedButton;

	private SpriteRenderer sr;

	bool activate;

	// Use this for initialization
	void Start ()
	{
		sr = GetComponent<SpriteRenderer>();
		Deactivate();
	}

	// Update is called once per frame
	void Update()
	{
		if(NetworkManager.GetNetworkInstance().MyReady || !NetworkManager.GetNetworkInstance().OtherReady)
		{
			Deactivate();
		}

		else
		{
			Activate();
		}
	}

	void Deactivate()
	{
		sr.sprite = deactivatedButton;
		activate = false;
	}

	void Activate()
	{
		sr.sprite = activatedButton;
		activate = true;
	}

	void OnMouseDown()
	{
		if (activate)
		{
			audio.Play();
		}
		if (!NetworkManager.GetNetworkInstance().MyReady && NetworkManager.GetNetworkInstance().OtherReady)
		{
			LobbyAndRoomManager.GetLobbyAndRoomManager ().ChangeStateToReady ();
		}
		else
		{
			LobbyAndRoomManager.GetLobbyAndRoomManager ().ChangeStateToWait ();
		}
	}
}
