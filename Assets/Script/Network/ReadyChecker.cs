using UnityEngine;
using System.Collections;

public class ReadyChecker : MonoBehaviour {

	public static ReadyChecker readyCheckerInstance = null;

	public static ReadyChecker GetReadyChecker()
	{
		return readyCheckerInstance;
	}

	void Awake ()
	{
		readyCheckerInstance = this;
	}

	void Update ()
	{
		if(NetworkManager.GetNetworkInstance().MyReady == true && NetworkManager.GetNetworkInstance().OtherReady == true)
		{
			NoticeText.noticeText = "Loading select scene. Wait a minute.";
			StartCoroutine("Loading");
//			CS_WaitingImage.CallWaitingImage();
//			Application.LoadLevel("TeamSelect_M_inProcess");
//			Destroy (gameObject);
		}
	}

	IEnumerator Loading()
	{
		CS_WaitingImage.CallWaitingImage();
		yield return new WaitForSeconds(2.0f);
		Application.LoadLevel("TeamSelect_M_inProcess");
		Destroy (gameObject);
	}
}
