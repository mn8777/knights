using UnityEngine;
using System.Collections;

public class JoinLocalRoomButton : MonoBehaviour {

	void OnMouseDown()
	{
		LobbyAndRoomManager.GetLobbyAndRoomManager().JoinLocalRoom ();
	}
}
