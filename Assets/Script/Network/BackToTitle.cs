﻿using UnityEngine;
using System.Collections;

public class BackToTitle : MonoBehaviour {

	void OnMouseDown()
	{
		audio.Play();
		LobbyAndRoomManager.GetLobbyAndRoomManager().LeaveLobby();
	}
}
