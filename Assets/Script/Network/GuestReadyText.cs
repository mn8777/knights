﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GuestReadyText : MonoBehaviour {

	string isReady (bool readyState)
	{
		if (readyState)
			return "Ready";
		else
			return "Wait";
	}

	// Update is called once per frame
	void Update () {
		if (Network.isServer)
		{
			if (Network.connections.Length > 0)
			{
				GetComponent<TextMesh> ().text =
				"Guest\nPlayer 2 : " + isReady (NetworkManager.GetNetworkInstance().OtherReady);
			}
			else
			{
				GetComponent<TextMesh> ().text =
				"Guest\nPlayer 2 : Disconnected";
			}
		}

		else
		{
			GetComponent<TextMesh> ().text =
			"Guest\nPlayer 2 : " + isReady (NetworkManager.GetNetworkInstance().MyReady);
		}
	}
}
