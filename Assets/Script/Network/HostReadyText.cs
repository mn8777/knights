﻿using UnityEngine;
using System.Collections;

public class HostReadyText : MonoBehaviour {

	string isReady (bool readyState)
	{
		if (readyState)
			return "Ready";
		else
			return "Wait";
	}

	// Update is called once per frame
	void Update () {
		if (Network.isServer)
		{
			GetComponent<TextMesh> ().text =
			"Host (" + NetworkInfo.myIp + ")\nPlayer 1 : " + isReady (NetworkManager.GetNetworkInstance().MyReady);
		}
		else
		{
			if (Network.connections.Length > 0)
			{
				GetComponent<TextMesh> ().text =
				"Host (" + NetworkInfo.othersIp + ")\nPlayer 1 : " + isReady (NetworkManager.GetNetworkInstance().OtherReady);
			}
			else
			{
				GetComponent<TextMesh> ().text =
				"Host (" + NetworkInfo.othersIp + ")\nPlayer 1 : DisConnected";
			}
		}
	}
}
