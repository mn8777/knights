﻿using UnityEngine;
using System.Collections;

public class JoinRoomButton : MonoBehaviour {

	void OnMouseDown ()
	{
		audio.Play();
		LobbyAndRoomManager.GetLobbyAndRoomManager().JoinRoom();
	}
}
 