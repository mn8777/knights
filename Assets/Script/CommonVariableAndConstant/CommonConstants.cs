﻿using UnityEngine;
using System.Collections;

namespace CommonConstants
{
	public static class Constants
	{
		public const int ofTeamMembers = 4;
		public const int ofCharactersInEachTeam = 10;

		//캐릭터 포지션Z의 차등값. 이것으로 겹침문제를 방지한다
		public const float diffPositionZ = 0.02f;

		//체력과 공격력의 표준값과 틱당 변화량
		public const int StandardHP = 300;
		public const int TickHP = 45;
		public const int StandardAttack = 50;
		public const int TickAttack = 8;

		//메시지가 처음 만들어지는 포지션X
		public const float MessageSpawnPositionX = -63.7f;
		public const float MessageSpawnPositionY = 26.3f;
		public const float MessageSpawnPositionZ = -4.0f;

		//1P와 2P의 캐릭터가 생성되는 X축 위치
		public const int xPosition1P = 3;
		public const int xPosition2P = 13;
		//얼마나 빨리 이동할 것인가
		public const float MoveAniSpeed = 10;

		public const float timeToHitAni = 0.3f;
		//알림메시지 및 컷씬이 지나가는 속도
		public const float MessageSpeed = 150;
		public const float CutSceneSpeed = 200;
		//돌(지형지물)의 생성 개수
		public const int RockCount = 5;

		//버프를 먹고 흡혈효과가 지속되는 시간. 턴종료를 할 때마다 1씩 줄어드므로 양쪽이 한번씩 움직이고 나면 2가 줄어든다.
		public const int BuffPowerDuration = 0;
		//버프를 먹고 나서 다음 번에 나올 때까지의 시간.
		public const int BuffSpawnDuration = 6;

		//캐릭터가 처음에 소환될 수 있는 Y위치 범위의 양쪽 끝값.
		public const int MinCharacterPositionY = 3;
		public const int MaxCharacterPositionY = 13;

		//버프가 소환될 수 있는 X위치 범위의 양쪽 끝값.
		public const int MinBuffPositionX = 6;
		public const int MaxBuffPositionX = 10;

		//전장의 가로세로 크기
		public const int MapSize = 15;

		public const float TileDistanceX = 3.3f;
		public const float TileDistanceY = 3.3f;

		public const int SkillChargeMax = 200;
		public const int DamagePerSkillCharge = 3;

		public const float ScaleOfHpBar = 1.0f;
		public const float ScaleOfSkillBar = 1;
	}
}