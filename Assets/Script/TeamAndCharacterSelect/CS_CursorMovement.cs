using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class CS_CursorMovement : MonoBehaviour {
	
	private	float cursorXPosition;
	private float cursorYPosition;
	// Use this for initialization
	void Start () {
		cursorYPosition = transform.position.y;
	}

	void Hide ()
	{
		transform.position = new Vector3 (0.0f, 0.0f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(Selected.current<Constants.ofCharactersInEachTeam){
			if(Mathf.Abs(cursorXPosition-(Selected.current * 0.97f - 1.85f))>0.1f){
				audio.Play();
			}
			cursorXPosition = (Selected.current * 0.97f - 1.85f);
			transform.position= new Vector3(cursorXPosition,cursorYPosition,-1.0f);
		}
		else{
			Hide ();
		}
	}
}
