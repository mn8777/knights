﻿using UnityEngine;
using System.Collections;

namespace TeamsAndCharacters
{
	public enum Teams
	{
		Pintos,
		Haskell
	}

	public enum PintosCharacter
	{
		Acht,
		Blitz,
		Julius,
		Lancinel,
		Naura,
		Py,
		Ruby,
		Sera,
		Sesto,
		Zen
	}

	public enum HaskellCharacter
	{
		Airy,
		Anna,
		Arcadi,
		Caml,
		Michell,
		Renard,
		Roshyanak,
		Schnell,
		Suyeon,
		Tigres
	}

	public enum Directions
	{
		Right,
		Up,
		Left,
		Down,
	}
}
