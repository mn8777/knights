﻿using UnityEngine;
using System.Collections;
using CommonConstants;

public class CS_SelectedCharacters : MonoBehaviour 
{	
	public static bool clickedCharacter=false;
	public static bool[] clickedSelectedCharacter = new bool[Constants.ofTeamMembers];
	public static int numberOfSelectedCharacters=0;
	public static int[] slot = new int[Constants.ofTeamMembers];

	public static void ResetStaticVariables()
	{
		clickedCharacter = false;
		numberOfSelectedCharacters = 0;
	}
	
	// Use this for initialization
	void Start () 
	{
		for(int i = 0; i < Constants.ofTeamMembers ; i ++)
		{
			slot[i] = Constants.ofCharactersInEachTeam; //anybignumber
			clickedSelectedCharacter[i]=false;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(clickedCharacter)
		{
			for(int i = 0; i < Constants.ofTeamMembers ; i ++)
			{
				if(slot[i]==Constants.ofCharactersInEachTeam)
				{
					slot[i]=Selected.current;
					Selected.character[i]=slot[i];
					clickedCharacter=false;
					numberOfSelectedCharacters++;
					break;
				}
			}
		}
		
		for(int i = 0; i < Constants.ofTeamMembers ; i ++){
			if(clickedSelectedCharacter[i]){
				Selected.character[i]=Constants.ofCharactersInEachTeam;
				slot[i]=Constants.ofCharactersInEachTeam;
				numberOfSelectedCharacters--;
				clickedSelectedCharacter[i]=false;
				break;
			}
		}
	}
}
	