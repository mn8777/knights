﻿using UnityEngine;
using System.Collections;

public class CS_WaitingImage : MonoBehaviour {

	public Sprite[] waitingImage;
	public static int waitingImageNumber;
	private SpriteRenderer sr;

	// Use this for initialization
	void Start () {
		waitingImageNumber = (int)Random.Range (0, 19);
		sr = GetComponent<SpriteRenderer> ();
	}

	// Update is called once per frame
	void Update () {
		sr.sprite = waitingImage [waitingImageNumber];
	}

	public static void CallWaitingImage () {
		Camera.main.transform.position = new Vector3 (-20, 0, -10);
	}


}
