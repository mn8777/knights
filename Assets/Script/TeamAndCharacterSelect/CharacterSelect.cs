using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class CharacterSelect : MonoBehaviour
{
	public PintosCharacter pintosCharacterForThisSlot;
	public HaskellCharacter haskellCharacterForThisSlot;
	public bool activate = false;

	void OnMouseOver()
	{
		if(GameManager.myTeam == Teams.Haskell)
		{
			Selected.current = (int)haskellCharacterForThisSlot;
		}
		else if(GameManager.myTeam == Teams.Pintos)
		{
			Selected.current = (int)pintosCharacterForThisSlot;
		}
	}
	void OnMouseExit () 
	{
		Selected.current=Constants.ofCharactersInEachTeam; 
	}
	
	void OnMouseDown()
	{
		if(CS_SelectedCharacters.numberOfSelectedCharacters<Constants.ofTeamMembers)
		{
			CS_SelectedCharacters.clickedCharacter=true;
			Deactivate();
		}
	}
	
	void Update ()
	{
		if(activate)
		{
			Activate ();
			activate=false;
		}
	}
	
	void Deactivate()
	{
		gameObject.renderer.material.color = Color.gray;
		gameObject.collider.enabled=false;
	}
	
	void Activate () 
	{
		gameObject.renderer.material.color = Color.white;
		gameObject.collider.enabled=true;
	}
}