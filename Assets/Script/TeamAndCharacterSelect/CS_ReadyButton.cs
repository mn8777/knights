﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class CS_ReadyButton : MonoBehaviour {

	public Sprite deactivatedReadyButton;
	public Sprite activatedReadyButton;
	public Sprite clickedReadyButton;
	public Sprite deactivatedStartButton;
	public Sprite activatedStartButton;
	public Sprite clickedStartButton;

	private SpriteRenderer sr;
	public static bool clicked;

	// Use this for initialization
	void Start () 
	{
		sr = GetComponent<SpriteRenderer>();
		clicked = false;
		Deactivate();
	}
	
	// Update is called once per frame
	void Update()
	{
		if(CS_SelectedCharacters.numberOfSelectedCharacters==Constants.ofTeamMembers && !clicked)
		{
			if(Network.isServer && NetworkManager.isClientReady)
			{
				Activate();
			}
			else if(Network.isClient)
			{
				Activate();
			}
		}
		else if(CS_SelectedCharacters.numberOfSelectedCharacters!=Constants.ofTeamMembers && !clicked)
		{
			Deactivate();	
		}
	
	}
	
	void OnMouseDown()
	{
		Clicked ();
		for(int i = 0; i<Constants.ofTeamMembers; i++)
		{
			GameManager.mySelectedCharacters[i] = Selected.character[i];
		}
		NetworkManager.SendSelectedCharacters (Selected.character);

		if (Network.isServer) 
		{
			NetworkManager.CallStartInGame();
		}
		else if(Network.isClient)
		{
			NetworkManager.SendClientReady();
		}

	}
	
	void Deactivate(){
		if (Network.isServer) {
			sr.sprite = deactivatedStartButton;
		} 
		else if (Network.isClient) {
			sr.sprite= deactivatedReadyButton;
		}
		gameObject.collider.enabled=false;
	}
	
	void Activate(){
		if (Network.isServer) {
			sr.sprite = activatedStartButton;
		} 
		else if (Network.isClient) {
			sr.sprite= activatedReadyButton;
		}
		gameObject.collider.enabled=true;
	}

	void Clicked(){
		clicked = true;
		audio.Play ();
		if (Network.isServer) {
			sr.sprite = clickedStartButton;
		} 
		else if (Network.isClient) {
			sr.sprite= clickedReadyButton;
		}
		gameObject.collider.enabled=false;
	}

}
