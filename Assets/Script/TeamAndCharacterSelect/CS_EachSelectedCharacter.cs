﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class CS_EachSelectedCharacter : MonoBehaviour {

	public int slot;
	public Sprite[] pintos;
	public Sprite[] haskell;
	public GameObject canceledCharacter;
	
	private SpriteRenderer sr;
	private Animator anim;
	private int selected = -1;

	void Start () 
	{
		sr = GetComponent<SpriteRenderer>();
		anim = GetComponent<Animator>();
	}

	void Update () 
	{
		if(selected != Selected.character[slot]){
			if (GameManager.myTeam == Teams.Haskell)
			{
				sr.sprite=haskell[Selected.character[slot]];
			}
			else if (GameManager.myTeam == Teams.Pintos)
			{
				sr.sprite=pintos[Selected.character[slot]];
			}
			selected = Selected.character[slot];
			anim.SetTrigger("selected");
		}
		if (Network.isClient && CS_ReadyButton.clicked) {
			gameObject.collider.enabled=false;
		}
		//10 = empty
	}
	
	void OnMouseDown() 
	{
		canceledCharacter=GameObject.Find (Selected.character[slot].ToString());
		canceledCharacter.GetComponent<CharacterSelect>().activate=true;
		Debug.Log (canceledCharacter);
		CS_SelectedCharacters.clickedSelectedCharacter[slot]=true;
		anim.SetTrigger("diselected");
	}
}
