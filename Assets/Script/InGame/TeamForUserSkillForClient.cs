﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class TeamForUserSkillForClient : MonoBehaviour {

	public Sprite[] pintos;
	public Sprite[] haskel;
	
	private SpriteRenderer sr;
	
	// Use this for initialization
	void Start () 
	{
		sr = GetComponent<SpriteRenderer>();
		if (Network.isServer) {
			if (GameManager.otherTeam == Teams.Pintos) {
								sr.sprite = pintos[0];
						} else if (GameManager.otherTeam == Teams.Haskell) {
								sr.sprite = haskel[0];
						}
		} 
		else if (Network.isClient) {
			if (GameManager.myTeam == Teams.Pintos) {
								sr.sprite = pintos[0];
						} else if (GameManager.myTeam == Teams.Haskell) {
								sr.sprite = haskel[0];
						}
				}
	}

	void Update ()
	{
		if (Network.isServer) {
			if(GameManager.enemyUserSkillCharge<100){
				if (GameManager.otherTeam == Teams.Pintos) {
					sr.sprite = pintos[0];
				} 
				else if (GameManager.otherTeam == Teams.Haskell) {
					sr.sprite = haskel[0];
				}
			}
			else if(GameManager.enemyUserSkillCharge>=100&&GameManager.myUserSkillCharge<200){
				if (GameManager.otherTeam == Teams.Pintos) {
					sr.sprite = pintos[1];
				} 
				else if (GameManager.otherTeam == Teams.Haskell) {
					sr.sprite = haskel[1];
				}
			}
			else if(GameManager.enemyUserSkillCharge==200){
				if (GameManager.otherTeam == Teams.Pintos) {
					sr.sprite = pintos[2];
				} 
				else if (GameManager.otherTeam == Teams.Haskell) {
					sr.sprite = haskel[2];
				}
			}
		}
		else if (Network.isClient){
			if(GameManager.myUserSkillCharge<100){
				if (GameManager.myTeam == Teams.Pintos) {
					sr.sprite = pintos[0];
				} 
				else if (GameManager.myTeam == Teams.Haskell) {
					sr.sprite = haskel[0];
				}
			}
			else if(GameManager.myUserSkillCharge>=100&&GameManager.myUserSkillCharge<200){
				if (GameManager.myTeam == Teams.Pintos) {
					sr.sprite = pintos[1];
				} 
				else if (GameManager.myTeam == Teams.Haskell) {
					sr.sprite = haskel[1];
				}
			}
			else if(GameManager.myUserSkillCharge==200){
				if (GameManager.myTeam == Teams.Pintos) {
					sr.sprite = pintos[2];
				} 
				else if (GameManager.myTeam == Teams.Haskell) {
					sr.sprite = haskel[2];
				}
			}
		}
	}
}
