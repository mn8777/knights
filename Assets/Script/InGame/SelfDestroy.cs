﻿using UnityEngine;
using System.Collections;

public class SelfDestroy : MonoBehaviour {
	public float timer = 1.5f;
	// Use this for initialization
	void Start () {
		Invoke ("destroySelf", timer);
	}
	void destroySelf(){
		Destroy (gameObject);
	}
}
