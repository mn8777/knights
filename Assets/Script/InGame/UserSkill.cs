using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class UserSkill : MonoBehaviour
{
	public static AudioSource SE = null;

	public static AudioClip pintosUserSkillSound = null;
	public static AudioClip haskellUserSkillSound = null;
	public static AudioClip hitSound = null;
	public GameObject[] userskills;
	public static GameObject[] userskillsStatic;
	private static GameObject userskillInstance;

	void Awake()
	{
		SE = GetComponent<AudioSource>();
		pintosUserSkillSound = (AudioClip)Resources.Load("explosion");
		haskellUserSkillSound = (AudioClip)Resources.Load("twinkle2");
		hitSound = (AudioClip)Resources.Load("punch");
		userskillsStatic = userskills;
	}


	IEnumerator OnMouseDown()
	{
		Debug.Log ("hi");
		if(GameManager.Turn && GameManager.GlobalTurnPhase == GameManager.TurnPhases.Wait)
		{
			int SkillPower = 0;

			if(GameManager.myUserSkillCharge >= 100 && GameManager.myUserSkillCharge != 200)
			{
				SkillPower = 1;
			}
			else if(GameManager.myUserSkillCharge == 200)
			{
				SkillPower = 2;
			}

			if(SkillPower != 0)
			{
				foreach(Character Knight in GameManager.Characters)
				{
					if (Knight == null)
					{
						continue;
					}
					if(GameManager.myTeam == Teams.Pintos && GameManager.isMyCharacter(Knight) == false)
					{
						userskillInstance = Instantiate(userskillsStatic[0]) as GameObject;
						SE.PlayOneShot(pintosUserSkillSound, 1.0F);
						GameManager.ApplyDamage(Knight, 40*SkillPower);
						SE.PlayOneShot(hitSound, 0.7F);
					}
					else if(GameManager.myTeam == Teams.Haskell && GameManager.isMyCharacter(Knight) == true)
					{
						userskillInstance = Instantiate(userskillsStatic[1],Knight.transform.position,Quaternion.identity) as GameObject;
						Debug.Log(Knight.transform.position);
						SE.PlayOneShot(haskellUserSkillSound, 1.0F);
						GameManager.ApplyHaskellBuff(Knight, SkillPower);
					}
				}
			}

			GameManager.myUserSkillCharge -= 100*SkillPower;

			NetworkManager.SendUserSkillActivation();
		}

		yield return new WaitForEndOfFrame ();

		if(GameManager.aliveCharacterCount(true) == 0 || GameManager.aliveCharacterCount(false) == 0)
		{
			NoticeMessage Message = Instantiate (GameManager.StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, 26.3f, -4), Quaternion.identity) as NoticeMessage;
			Message.GetComponent<SpriteRenderer> ().sprite = Message.GameEnd;
			Debug.Log ("All characters retired.");
		}
	}

	public static IEnumerator ActivateEnemyUserSkill()
	{
		int SkillPower = 0;

		if(GameManager.enemyUserSkillCharge >= 100 && GameManager.enemyUserSkillCharge != 200)
		{
			SkillPower = 1;
		}
		else if(GameManager.enemyUserSkillCharge == 200)
		{
			SkillPower = 2;
		}

		if(SkillPower != 0)
		{
			foreach(Character Knight in GameManager.Characters)
			{
				if (Knight == null)
				{
					continue;
				}

				if(GameManager.otherTeam == Teams.Pintos && GameManager.isMyCharacter(Knight) == true)
				{
					userskillInstance = Instantiate(userskillsStatic[0]) as GameObject;
					SE.PlayOneShot(pintosUserSkillSound, 1.0F);
					GameManager.ApplyDamage(Knight, 40*SkillPower);
					SE.PlayOneShot(hitSound, 0.7F);
				}
				else if(GameManager.otherTeam == Teams.Haskell && GameManager.isMyCharacter(Knight) == false)
				{
					userskillInstance = Instantiate(userskillsStatic[1],Knight.transform.position,Quaternion.identity) as GameObject;
					Debug.Log(Knight.transform.position);
					SE.PlayOneShot(haskellUserSkillSound, 1.0F);
					GameManager.ApplyHaskellBuff(Knight, SkillPower);
				}
			}
		}

		GameManager.enemyUserSkillCharge -= 100*SkillPower;

		yield return new WaitForEndOfFrame ();

		if(GameManager.aliveCharacterCount(true) == 0 || GameManager.aliveCharacterCount(false) == 0)
		{
			NoticeMessage Message = Instantiate (GameManager.StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, 26.3f, -4), Quaternion.identity) as NoticeMessage;
			Message.GetComponent<SpriteRenderer> ().sprite = Message.GameEnd;
			Debug.Log ("All characters retired.");
		}
	}
}