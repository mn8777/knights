﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class FaceMouseOverUI : MonoBehaviour
{
	
	public int faceNumber;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseOver() {
		CharacterInfoSheetInGameUI.viewedInfoSheet = faceNumber;
	}
	void OnMouseExit() {
		CharacterInfoSheetInGameUI.viewedInfoSheet = Constants.ofCharactersInEachTeam*2;
	}
}
