﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;
using CommonConstants;

public class GameManager : MonoBehaviour
{
	private AudioSource se;
	public AudioClip footStepSound;

	public static int[] mySelectedCharacters = new int[Constants.ofTeamMembers];
	public static int[] otherSelectedCharacters = new int[Constants.ofTeamMembers];

	public string[] Names;
	public static string[] staticNames;

	public static int finishedCharacters;

	public Tile TilePf;
	public Character CharacterPrefab;
	public static List<Character> Characters = new List<Character>();
	public static List<Tile> Tiles = new List<Tile>();
	static List<Tile> TilesForPlayer1 = new List<Tile>();
	static List<Tile> TilesForPlayer2 = new List<Tile>();
	static List<Tile> TilesForBuffSpawn = new List<Tile>();

	public static bool Turn;

	public static Teams myTeam;
	public static Teams otherTeam;

	public static Character MovingCharacter;

	public Buff BuffPrefab;
	public static Buff StaticBuffPrefab;
	public GameObject Rock;

	static int BuffSpawnTime;

	public static int myUserSkillCharge;
	public static int enemyUserSkillCharge;

	public List<Vector2> MoveLevel1;
	public List<Vector2> MoveLevel2;
	public List<Vector2> MoveLevel3;
	public List<Vector2> MoveLevel4;
	public List<Vector2> MoveLevel5;
	public List<Vector2> MoveLevel6;
	public List<Vector2> MoveLevel7;
	public List<Vector2> MoveLevel8;
	public List<Vector2> MoveLevel9;
	public List<Vector2> MoveLevel10;

	public List<List<Vector2>> MoveLevels = new List<List<Vector2>>();

	public NoticeMessage MessagePrefab;
	public static NoticeMessage StaticMessagePrefab;

	public TurnEffect[] TurnEffects;
	public static TurnEffect[] StaticTurnEffects;
	public GameObject healEffect;
	public static GameObject healEffectStatic;

	public static List<Character> Attackers = new List<Character>();

	public CutScene CutScene2;
	public CutScene CutScene3;
	public CutScene CutScene4;

	public InGameUI[] InGameUIs;

	public enum TurnPhases
	{
		Wait,
		Move,
		Direction,
	}
	public static TurnPhases GlobalTurnPhase;

	public static void ResetStaticVariables()
	{
		ClearIntegerArray (mySelectedCharacters);
		ClearIntegerArray (otherSelectedCharacters);

		finishedCharacters = 0;
		myUserSkillCharge = 0;
		enemyUserSkillCharge = 0;

		Characters.Clear ();
		Tiles.Clear ();
		TilesForPlayer1.Clear ();
		TilesForPlayer2.Clear ();
        TilesForBuffSpawn.Clear ();
	}

	public static void ClearIntegerArray(int[] Array)
	{
		for(int i = 0; i<Array.Length; i++)
		{
			Array[i] = 0;
		}
	}

	void Awake()
	{
		se = GetComponent<AudioSource>();
		healEffectStatic = healEffect;
	}

	void Start()
	{
		DontDestroyOnLoad (gameObject);

		BGMManager bgmPlayer = FindObjectOfType(typeof(BGMManager)) as BGMManager;
		if (bgmPlayer != null)
		{
			bgmPlayer.ChangeBgmAtBattle();
		}

		//10(9단계에서 수정)단계의 이동 범위를 불러올 수 있게 세팅함
		MoveLevels.Add (MoveLevel1);
		MoveLevels.Add (MoveLevel2);
		MoveLevels.Add (MoveLevel3);
		MoveLevels.Add (MoveLevel4);
		MoveLevels.Add (MoveLevel5);
		MoveLevels.Add (MoveLevel6);
		MoveLevels.Add (MoveLevel7);
		MoveLevels.Add (MoveLevel8);
		MoveLevels.Add (MoveLevel9);
		MoveLevels.Add (MoveLevel10);

		Random.seed = PlayerPrefs.GetInt ("randomSeed");

		StaticMessagePrefab = MessagePrefab;
		StaticTurnEffects = TurnEffects;
		StaticBuffPrefab = BuffPrefab;
		staticNames = Names;

		NoticeMessage Message = Instantiate(MessagePrefab, new Vector3(Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
		Message.GetComponent<SpriteRenderer>().sprite = Message.GameStart;

		BuffSpawnTime = 2;

		if(Network.isServer)
		{
			Turn = true;
		}
		else
		{
			Turn = false;
		}

		SpawnTiles ();
		SpawnKnights();
		SpawnRocks();

		foreach(TurnEffect turnEffect in TurnEffects)
		{
			turnEffect.DoSetting();
		}
	}

	//RPC를 통해 상대가 고른 팀이 어느 쪽인지를 받아온다. 팀을 고르는 버튼에 Send기능 달려있음
	public static void SetOtherTeamInfo(string TeamInfo)
	{
		if(TeamInfo == "Pintos")
		{
			otherTeam = Teams.Pintos;
		}
		else if(TeamInfo == "Haskell")
		{
			otherTeam = Teams.Haskell;
		}
		else
		{
			Debug.Log("Other Player Selected "+TeamInfo+" Team.");
			Debug.LogError("Invalid OtherPlayer's Team Info.");
		}
	}

	//캐릭터 선택이 끝났으면 true를 반환. int어레이를 투입하므로 자신과 상대 각각 따로따로 체크할 수 있다
	bool isCharacterSelectionFinished(List<int> selectedNumbers)
	{
		int result = 1;

		foreach(int selectedNumber in selectedNumbers)
		{
			result *= selectedNumber;
		}

		if(result == 0)
		{
			return false;
		}

		else
		{
			return true;
		}
	}

	//캐릭터 선택창에서 양쪽 플레이어가 각각 4명 다 골랐는지 체크, 확인되면 인게임씬으로 넘어간다.
	void CheckSelectFinished()
	{
		if(Application.loadedLevelName == "CharacterSelect")
		{
			bool Finished = true;
			foreach(int Selection in mySelectedCharacters)
			{
				if(Selection == 0)
				{
					Finished = false;
				}
			}
			foreach(int Selection in otherSelectedCharacters)
			{
				if(Selection == 0)
				{
					Finished = false;
				}
			}

			if(Finished == true)
			{
				NetworkManager.CallStartInGame();
			}
		}
	}

	public static IEnumerator ApplyDamageForAll ()
	{
		bool isAnyDeath = false;
		foreach(Character Knight in Characters)
		{
			if(isMyCharacter(Knight) != Turn && Knight.totalDamage != 0)
			{
				ApplyDamage(Knight, Knight.totalDamage);
				isAnyDeath = true;
			}
		}

		Buff BuffObject = GameObject.FindObjectOfType (typeof(Buff)) as Buff;
		if(BuffObject != null && BuffObject.CharactersForAttack.Count != 0)
		{
			BuffObject.HP -= BuffObject.totalDamage;
			if(BuffObject.HP <= 0)
			{
				Destroy (BuffObject.gameObject);

				foreach(Tile tile in TilesForBuffSpawn){
					tile.BuffOnTile = null;
				}//delete buff ref in tile

				foreach(Character Knight in Characters)
				{
					if(isMyCharacter(Knight) == isMyCharacter(Attackers[0]))
					{
						Knight.ObjectBuffDuration = Constants.BuffPowerDuration;
					}
				}

				BuffSpawnTime = Constants.BuffSpawnDuration;
			}
		}
		if(isAnyDeath) yield return new WaitForSeconds(1.5f);
		else yield return new WaitForEndOfFrame();

		EndTurn();
	}

	public static void ApplyDamage(Character Knight, int Damage)
	{
		if(Knight != null)
		{
			Knight.HP -= Damage;
			Knight.GetComponent<Animator>().SetTrigger("Hit");

			if(Knight.HP <= 0)
			{
				Knight.HP = 0;
				Knight.CurrentTile.CharacterOnTile = null;
//				Destroy (Knight.gameObject);
				Knight.gameObject.AddComponent<SelfDestroy>().timer = 1.2f;
				Animator knightAnim = Knight.GetComponent<Animator>();
				knightAnim.SetTrigger ("Die");
				/*if(aliveCharacterCount(isMyCharacter(Knight)) == 1)
				{
					NoticeMessage Message = Instantiate (GameManager.StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
					Message.GetComponent<SpriteRenderer> ().sprite = Message.GameEnd;
					Destroy(Knight.gameObject);
					Debug.Log ("All characters retired.");
					return;
				}
				else
				{

				}*/
			}

			if(isMyCharacter(Knight))
			{
				myUserSkillCharge += Damage/Constants.DamagePerSkillCharge;

				if(myUserSkillCharge > Constants.SkillChargeMax)
				{
					myUserSkillCharge = Constants.SkillChargeMax;
				}
			}
			else
			{
				enemyUserSkillCharge += Damage/Constants.DamagePerSkillCharge;

				if(enemyUserSkillCharge > Constants.SkillChargeMax)
				{
					enemyUserSkillCharge = Constants.SkillChargeMax;
				}
			}
		}
	}

	public static void PutHeal(Character Knight, int Heal)
	{
		Knight.HP += Heal;
		GameObject healInstance = Instantiate(healEffectStatic,Knight.gameObject.transform.position,Quaternion.identity) as GameObject;
		healInstance.AddComponent<SelfDestroy>();

		if(Knight.HP > Knight.MaxHP)
		{
			Knight.HP = Knight.MaxHP;
		}
	}

	public static void EndTurn()
	{
		MovingCharacter = null;
		finishedCharacters = 0;

		foreach(Character Knight in Characters)
		{
			if(Knight != null)
			{
				Knight.Activated = true;
				Knight.GetComponent<Animator>().SetBool("Done",false);
				Knight.ShowIfMyTeam.enabled = isMyCharacter(Knight) && !Turn;

				if(Knight.ObjectBuffDuration > 0)
				{
					Knight.ObjectBuffDuration -= 1;
				}

				Knight.Attack = Constants.TickAttack * Knight.IndexAttack [Knight.CharacterIndex] + Constants.StandardAttack;
				Knight.Move = Knight.IndexMove [Knight.CharacterIndex];
				Knight.totalDamage = 0;
				if(Knight.DamageText != null)
				{
					Knight.DamageText.text = "";
				}
				Knight.HitObjects.Clear();
				Knight.CharactersForAttack.Clear();

				Knight.DisappearWing();
			}
		}

		DeactivateAllTiles ();

		Debug.Log ("Endturn - Alive My Characters : " + aliveCharacterCount (true));
		Debug.Log ("Endturn - Alive Enemy Characters : " + aliveCharacterCount (false));

		string aliveCharacters = "";
		foreach(Character Knight in Characters)
		{
			if(Knight != null)
			{
				aliveCharacters += staticNames[Knight.CharacterIndex]+", ";
			}
		}
		Debug.Log ("Endturn - Alive Characters : " + aliveCharacters);

		if(aliveCharacterCount(true) == 0 || aliveCharacterCount(false) == 0)
		{
			NoticeMessage Message = Instantiate (GameManager.StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
			Message.GetComponent<SpriteRenderer> ().sprite = Message.GameEnd;
			Debug.Log ("All characters retired.");
		}
		else
		{
			ChangeTurn();
			ShowTurnMessage();
		}

		BuffSpawnTime -= 1;

		if(buffExistence())
		{
			Buff BuffObject = GameObject.FindObjectOfType (typeof(Buff)) as Buff;
			BuffObject.totalDamage = 0;
			if(BuffObject.DamageText != null)
			{
				BuffObject.DamageText.text = "";
			}
			BuffObject.CharactersForAttack.Clear();
		}
		else if(BuffSpawnTime <= 0)
		{
			SpawnBuff();
		}

		foreach(TurnEffect turnEffect in StaticTurnEffects)
		{
			turnEffect.DoSetting();
		}

		Attackers.Clear ();
	}

	public static bool isMyCharacter(Character Knight)
	{
		if(Network.isServer)
		{
			if(Knight.GameCharacterIndex < Constants.ofTeamMembers)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(Knight.GameCharacterIndex < Constants.ofTeamMembers)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}

	static bool buffExistence()
	{
		bool Result = false;

		foreach(Tile tile in Tiles)
		{
			if(tile.BuffOnTile != null)
			{
				Result = true;
			}
		}

		return Result;
	}

	public void ApplyAttacks ()
	{
		Debug.Log ("Apply Attacks.");

		InteractableObject MostChainedObject = null;
		foreach(Character Knight in Characters)
		{
			if(Knight != null && isMyCharacter(Knight) != Turn)
			{
				if(MostChainedObject == null)
				{
					MostChainedObject = Knight;
				}
				else if(Knight.CharactersForAttack.Count > MostChainedObject.CharactersForAttack.Count)
				{
					MostChainedObject = Knight;
				}
			}
		}
		Buff BuffObject = GameObject.FindObjectOfType (typeof(Buff)) as Buff;
		if(BuffObject != null && MostChainedObject.CharactersForAttack.Count < BuffObject.CharactersForAttack.Count)
		{
			MostChainedObject = BuffObject;
		}
		Debug.Log ("MostChainedObject is " + MostChainedObject.name);

		string attackersOfMcc = "";
		foreach(Character Attacker in MostChainedObject.CharactersForAttack)
		{
			attackersOfMcc += Names[Attacker.CharacterIndex]+", ";
			Attackers.Add (Attacker);
		}
		Debug.Log ("Chain of MCC is " + Attackers.Count);
		Debug.Log ("Attackers of MCC are " + attackersOfMcc);

		foreach(Character Knight in Characters)
		{
			bool isAlreadyRegistered = false;

			foreach(Character Attacker in Attackers)
			{
				if(Knight == Attacker)
				{
					isAlreadyRegistered = true;
				}
			}

			if(!isAlreadyRegistered && Knight.HitObjects.Count != 0)
			{
				string hitObjectsNames = "";
				foreach(InteractableObject HitObject in Knight.HitObjects)
				{
					hitObjectsNames += HitObject.name+", ";
				}
				Attackers.Add (Knight);

				Debug.Log (Knight.name+"'s HitObjects : "+hitObjectsNames);
			}
		}
		Debug.Log ("Finally AttackerCount is " + Attackers.Count);

		if(Attackers.Count != 0)
		{
			if(MostChainedObject != null && MostChainedObject.CharactersForAttack.Count == 1)
			{
				Debug.Log ("Start DoAttack of "+Attackers[0].name+"!");
				StartCoroutine(Attackers[0].DoAttack(0));
			}
			else
			{
				CutScene CutSceneForSetting = null;
				if(MostChainedObject.CharactersForAttack.Count == 2)
				{
					CutSceneForSetting = Instantiate(CutScene2) as CutScene;
				}
				else if(MostChainedObject.CharactersForAttack.Count == 3)
				{
					CutSceneForSetting = Instantiate(CutScene3) as CutScene;
				}
				else if(MostChainedObject.CharactersForAttack.Count == 4)
				{
					CutSceneForSetting = Instantiate(CutScene4) as CutScene;
				}

				CutSceneForSetting.MostChainedObject = MostChainedObject;
				if(Turn)
				{
					CutSceneForSetting.Team = myTeam;
				}
				else
				{
					CutSceneForSetting.Team = otherTeam;
				}
			}
		}
		else
		{
			EndTurn();
		}
	}

	static void ChangeTurn()
	{
		if(Turn)
		{
			Turn = false;
		}
		else
		{
			Turn = true;
		}
	}

	public static void ShowTurnMessage()
	{
		if(Turn)
		{
			if (myTeam == Teams.Pintos)
			{
				NoticeMessage Message = Instantiate (StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
				Message.GetComponent<SpriteRenderer> ().sprite = Message.MyTurnPintos;
			}
			else
			{
				NoticeMessage Message = Instantiate (StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
				Message.GetComponent<SpriteRenderer> ().sprite = Message.MyTurnHaskell;
			}
		}
		else
		{
			if (otherTeam == Teams.Pintos)
			{
				NoticeMessage Message = Instantiate (StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
				Message.GetComponent<SpriteRenderer> ().sprite = Message.EnemyTurnPintos;
			}
			else
			{
				NoticeMessage Message = Instantiate (StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
				Message.GetComponent<SpriteRenderer> ().sprite = Message.EnemyTurnHaskell;
			}
		}
	}

	/*static void ChangeTurn_And_ShowTurnMessage ()
	{
		if (Turn)
		{
			Turn = false;

			if (otherTeam == Teams.Pintos)
			{
				NoticeMessage Message = Instantiate (StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
				Message.GetComponent<SpriteRenderer> ().sprite = Message.EnemyTurnPintos;
			}
			else
			{
				NoticeMessage Message = Instantiate (StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
				Message.GetComponent<SpriteRenderer> ().sprite = Message.EnemyTurnHaskell;
			}
		}
		else
		{
			Turn = true;

			if (myTeam == Teams.Pintos)
			{
				NoticeMessage Message = Instantiate (StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionY, Constants.MessageSpawnPositionZ), Quaternion.identity) as NoticeMessage;
				Message.GetComponent<SpriteRenderer> ().sprite = Message.MyTurnPintos;
			}
			else
			{
				NoticeMessage Message = Instantiate (StaticMessagePrefab, new Vector3 (Constants.MessageSpawnPositionX, Constants.MessageSpawnPositionYf, -4), Quaternion.identity) as NoticeMessage;
				Message.GetComponent<SpriteRenderer> ().sprite = Message.MyTurnHaskell;
			}
		}
	}*/

	void SpawnTiles ()
	{
		Debug.Log ("Spawn Tiles.");
		for (int y = 1; y <= Constants.MapSize; y++)
		{
			for (int x = 1; x <= Constants.MapSize; x++)
			{
				Tile TileForAddList = Instantiate (TilePf, new Vector3 (x * Constants.TileDistanceX, y * Constants.TileDistanceY -2, 2), Quaternion.identity) as Tile;
				TileForAddList.X = x;
				TileForAddList.Y = y;
				TileForAddList.Manager = this;
				TileForAddList.name = "Tile(" + TileForAddList.X + "," + TileForAddList.Y + ")";
				TileForAddList.transform.parent = transform;
				Tiles.Add (TileForAddList);

				if (x == Constants.xPosition1P && y >= Constants.MinCharacterPositionY && y <= Constants.MaxCharacterPositionY)
				{
					TilesForPlayer1.Add (TileForAddList);
				}
				else if (x == Constants.xPosition2P && y >= Constants.MinCharacterPositionY && y <= Constants.MaxCharacterPositionY)
				{
					TilesForPlayer2.Add (TileForAddList);
				}
				else if(x >= Constants.MinBuffPositionX && x <= Constants.MaxBuffPositionX)
				{
					TilesForBuffSpawn.Add (TileForAddList);
				}
			}
		}
	}

	void SpawnKnights ()
	{
		if(Network.isServer)
		{
			for (int i = 0; i < Constants.ofTeamMembers; i++)
			{
				int SpawnedTileNumber = UnityEngine.Random.Range (0, TilesForPlayer1.Count);
				Tile SpawnedTile = TilesForPlayer1[SpawnedTileNumber];
				Character CharacterForSetting = Instantiate (CharacterPrefab, SpawnedTile.transform.position + Character.relativePosition_AboutTile(SpawnedTile.Y), Quaternion.identity) as Character;
				CharacterForSetting.CharacterIndex = mySelectedCharacters [i]+return10IfHaskell(myTeam);
				CharacterForSetting.name = Names[CharacterForSetting.CharacterIndex];
				CharacterForSetting.Activated = true;
				CharacterForSetting.CurrentTile = TilesForPlayer1 [SpawnedTileNumber];
				CharacterForSetting.Manager = this;
				CharacterForSetting.Direction = Directions.Right;
				CharacterForSetting.GameCharacterIndex = i;
				Characters.Add (CharacterForSetting);
				TilesForPlayer1 [SpawnedTileNumber].CharacterOnTile = CharacterForSetting;
				TilesForPlayer1.RemoveAt (SpawnedTileNumber);
			}

			for (int i = 0; i < Constants.ofTeamMembers; i++)
			{
				int SpawnedTileNumber = UnityEngine.Random.Range (0, TilesForPlayer2.Count);
				Tile SpawnedTile = TilesForPlayer2[SpawnedTileNumber];
				Character CharacterForSetting = Instantiate (CharacterPrefab, SpawnedTile.transform.position + Character.relativePosition_AboutTile(SpawnedTile.Y), Quaternion.identity) as Character;
				CharacterForSetting.CharacterIndex = otherSelectedCharacters [i]+return10IfHaskell(otherTeam);
				CharacterForSetting.name = Names[CharacterForSetting.CharacterIndex];
				CharacterForSetting.CurrentTile = TilesForPlayer2 [SpawnedTileNumber];
				CharacterForSetting.Manager = this;
				CharacterForSetting.Direction = Directions.Left;
				Vector3 Scale = CharacterForSetting.transform.localScale;
				Scale.x = 1;
				CharacterForSetting.transform.localScale = Scale;
				if(CharacterForSetting.DamageText != null)
				{
					CharacterForSetting.DamageText.gameObject.transform.localScale = Scale;
				}
				CharacterForSetting.GameCharacterIndex = i + Constants.ofTeamMembers;
				Characters.Add (CharacterForSetting);
				TilesForPlayer2 [SpawnedTileNumber].CharacterOnTile = CharacterForSetting;
				TilesForPlayer2.RemoveAt (SpawnedTileNumber);
			}
		}
		else
		{
			for (int i = 0; i < Constants.ofTeamMembers; i++)
			{
				int SpawnedTileNumber = UnityEngine.Random.Range (0, TilesForPlayer1.Count);
				Tile SpawnedTile = TilesForPlayer1[SpawnedTileNumber];
				Character CharacterForSetting = Instantiate (CharacterPrefab, SpawnedTile.transform.position + Character.relativePosition_AboutTile(SpawnedTile.Y), Quaternion.identity) as Character;
				CharacterForSetting.CharacterIndex = otherSelectedCharacters [i]+return10IfHaskell(otherTeam);
				CharacterForSetting.name = Names[CharacterForSetting.CharacterIndex];
				CharacterForSetting.Activated = true;
				CharacterForSetting.CurrentTile = TilesForPlayer1 [SpawnedTileNumber];
				CharacterForSetting.Manager = this;
				CharacterForSetting.Direction = Directions.Right;
				CharacterForSetting.GameCharacterIndex = i;
				Characters.Add (CharacterForSetting);
				TilesForPlayer1 [SpawnedTileNumber].CharacterOnTile = CharacterForSetting;
				TilesForPlayer1.RemoveAt (SpawnedTileNumber);
			}

			for (int i = 0; i < Constants.ofTeamMembers; i++)
			{
				int SpawnedTileNumber = UnityEngine.Random.Range (0, TilesForPlayer2.Count);
				Tile SpawnedTile = TilesForPlayer2[SpawnedTileNumber];
				Character CharacterForSetting = Instantiate (CharacterPrefab, SpawnedTile.transform.position + Character.relativePosition_AboutTile(SpawnedTile.Y), Quaternion.identity) as Character;
				CharacterForSetting.CharacterIndex = mySelectedCharacters [i]+return10IfHaskell (myTeam);
				CharacterForSetting.name = Names[CharacterForSetting.CharacterIndex];
				CharacterForSetting.CurrentTile = TilesForPlayer2 [SpawnedTileNumber];
				CharacterForSetting.Manager = this;
				CharacterForSetting.Direction = Directions.Left;
				Vector3 Scale = CharacterForSetting.transform.localScale;
				Scale.x = 1;
				CharacterForSetting.transform.localScale = Scale;
				if(CharacterForSetting.DamageText != null)
				{
					CharacterForSetting.DamageText.gameObject.transform.localScale = Scale;
				}
				CharacterForSetting.GameCharacterIndex = i + Constants.ofTeamMembers;
				Characters.Add (CharacterForSetting);
				TilesForPlayer2 [SpawnedTileNumber].CharacterOnTile = CharacterForSetting;
				TilesForPlayer2.RemoveAt (SpawnedTileNumber);
			}
		}

		foreach (InGameUI UI in InGameUIs)
		{
			UI.SetCharacterNames();
		}
	}

	int return10IfHaskell(Teams team)
	{
		if(team == Teams.Pintos)
		{
			return 0;
		}
		else
		{
			return Constants.ofCharactersInEachTeam;
		}
	}

	void SpawnRocks()
	{
		for(int i = 0; i < Constants.RockCount; i++)
		{
			List<Tile> AvailableTiles = new List<Tile> ();
			foreach(Tile tile in Tiles)
			{
				if(tile.CharacterOnTile == null && tile.Occupied == false)
				{
					AvailableTiles.Add (tile);
				}
			}

			Tile SpawnedTile = AvailableTiles[UnityEngine.Random.Range(0, AvailableTiles.Count)];
			Instantiate(Rock, SpawnedTile.transform.position + Character.relativePosition_AboutTile(SpawnedTile.Y), Quaternion.identity);
			SpawnedTile.Occupied = true;
		}
	}

	static void SpawnBuff()
	{
		Debug.Log ("Sender Spawns Item.");

		List<Tile> AvailableTiles = new List<Tile> ();
		foreach(Tile tile in TilesForBuffSpawn)
		{
			if(tile.CharacterOnTile == null && tile.Occupied == false)
			{
				AvailableTiles.Add (tile);
			}
		}

		Tile SpawnedTile = AvailableTiles [UnityEngine.Random.Range (0, AvailableTiles.Count)];
		Buff BuffInstance = Instantiate (StaticBuffPrefab, SpawnedTile.transform.position + Character.relativePosition_AboutTile(SpawnedTile.Y), Quaternion.identity) as Buff;
		SpawnedTile.BuffOnTile = BuffInstance;
	}

	public static Tile GetTileAt(int X, int Y)
	{
		if(X >= 1 && X <= Constants.MapSize && Y >= 1 && Y<= Constants.MapSize)
		{
			return Tiles[Constants.MapSize*(Y-1)+X-1];
		}
		else
		{
			return null;
		}
	}

	public static Vector3 GetTilePositionAt(Vector2 inputVector)
	{
		return new Vector3 (3.3f * inputVector.x, 3.3f * inputVector.y - 2, Tiles [0].transform.position.z);
	}

	public static void DeactivateAllTiles()
	{
		foreach(Tile tile in Tiles)
		{
			tile.MyRenderer.sprite = tile.DefaultSprite;
			tile.Movable = false;
		}
	}

	public static void ChangeSpriteOfTiles(List<Tile> TileList, Sprite NewSprite)
	{
		foreach(Tile tile in TileList)
		{
			if(tile != null)
			{
				tile.MyRenderer.sprite = NewSprite;
			}
		}
	}

	public static void RegisterHitObject(List<Tile> TileList, Character Attacker)
	{
		foreach(Tile tile in TileList)
		{
			if(tile != null)
			{
				if(tile.BuffOnTile != null)
				{
					tile.BuffOnTile.CharactersForAttack.Add (Attacker);
					Attacker.HitObjects.Add (tile.BuffOnTile);
				}
				else if(tile.CharacterOnTile != null && isMyCharacter(tile.CharacterOnTile) != isMyCharacter(Attacker))
				{
					tile.CharacterOnTile.CharactersForAttack.Add (Attacker);
					Attacker.HitObjects.Add (tile.CharacterOnTile);
				}
			}
		}
	}

	public void MoveAndFix_Character_BeforeMoveAni(int CharacterIndex, int TileX, int TileY, int Direction)
	{
		Debug.Log ("Apply Received Move Information.");
		Character Knight = Characters [CharacterIndex];
		Knight.CurrentTile.CharacterOnTile = null;
		Knight.CurrentTile = GetTileAt (TileX, TileY);

		se.PlayOneShot(footStepSound, 1.0F);

		iTween.MoveTo(Knight.gameObject, 
			iTween.Hash(
				"position", Knight.CurrentTile.transform.position + Character.relativePosition_AboutTile(TileY), 
				"Speed", Constants.MoveAniSpeed, 
				"oncompletetarget", this.gameObject, 
				"oncomplete", "MoveAndFix_Character_AfterMoveAni", 
				"oncompleteparams", Knight)
			);

		Knight.CurrentTile.CharacterOnTile = Knight;
		Knight.Direction = IntegerToDirections (Direction);

		if(Direction == 1)
		{
			Vector3 Scale = Knight.transform.localScale;
			Scale.x = -1;
			Knight.transform.localScale = Scale;
			if(Knight.DamageText != null)
			{
				Knight.DamageText.gameObject.transform.localScale = Scale;
			}
		}
		else if(Direction == 3)
		{
			Vector3 Scale = Knight.transform.localScale;
			Scale.x = 1;
			Knight.transform.localScale = Scale;
			if(Knight.DamageText != null)
			{
				Knight.DamageText.gameObject.transform.localScale = Scale;
			}
		}
		DeactivateAllTiles ();
	}

	public void MoveAndFix_Character_AfterMoveAni(Character Knight)
	{
		se.Stop();

		List<Tile> AttackRange = ShowAndReturn_AttackRange (Knight);

		RegisterHitObject (AttackRange, Knight);
		DeactivateAllTiles ();
		Knight.Activated = false;

		finishedCharacters += 1;

		Debug.Log ("The Number of FinishedCharacters is " + finishedCharacters);
		Debug.Log ("Enemy's AliveCharacterCount is " + aliveCharacterCount (false));

		if(finishedCharacters == aliveCharacterCount(false))
		{
			NetworkManager.CallNextTurn();
		}
	}

	public static int aliveCharacterCount(bool forMyTeam)
	{
		int result = 0;
		string Members = "";

		foreach(Character Knight in GameManager.Characters)
		{
			if(Knight != null && GameManager.isMyCharacter(Knight) == forMyTeam)
			{
				Members += " "+staticNames[Knight.CharacterIndex];
				result += 1;
			}
		}
		/*if(Network.isServer == forMyTeam)
		{
			for(int i = 0; i<Constants.ofTeamMembers; i++)
			{
				if(Characters[i] != null)
				{
					result += 1;
				}
			}
		}
		else
		{
			for(int i = Constants.ofTeamMembers; i<Constants.ofTeamMembers*2; i++)
			{
				if(Characters[i] != null)
				{
					result += 1;
				}
			}
		}*/
		return result;
	}

	public List<Tile> ShowAndReturn_AttackRange(Character Attacker)
	{
		DeactivateAllTiles();
		List<Tile> AttackRange = new List<Tile>();
		List<Vector2> AttackableTiles = Attacker.AttackableTiles();
		Tile CurrentTile = Attacker.CurrentTile;

		foreach(Vector2 AttackableTile in AttackableTiles)
		{
			if(Attacker.Direction == Directions.Right)
			{
				AttackRange.Add (GetTileAt(CurrentTile.X+(int)(AttackableTile.x), CurrentTile.Y+(int)(AttackableTile.y)));
			}
			else if(Attacker.Direction == Directions.Up)
			{
				AttackRange.Add (GetTileAt(CurrentTile.X-(int)(AttackableTile.y), CurrentTile.Y+(int)(AttackableTile.x)));
			}
			else if(Attacker.Direction == Directions.Left)
			{
				AttackRange.Add (GetTileAt(CurrentTile.X-(int)(AttackableTile.x), CurrentTile.Y-(int)(AttackableTile.y)));
			}
			else if(Attacker.Direction == Directions.Down)
			{
				AttackRange.Add (GetTileAt(CurrentTile.X+(int)(AttackableTile.y), CurrentTile.Y-(int)(AttackableTile.x)));
			}
		}

		ChangeSpriteOfTiles(AttackRange, Attacker.CurrentTile.AttackableSprite);

		return AttackRange;
	}

	Directions IntegerToDirections(int Integer)
	{
		if(Integer == 1)
		{
			return Directions.Right;
		}
		else if(Integer == 2)
		{
			return Directions.Up;
		}
		else if(Integer == 3)
		{
			return Directions.Left;
		}
		else
		{
			return Directions.Down;
		}
	}

	public int DirectionToInteger(Directions Direction)
	{
		if(Direction == Directions.Right)
		{
			return 1;
		}
		else if(Direction == Directions.Up)
		{
			return 2;
		}
		else if(Direction == Directions.Left)
		{
			return 3;
		}
		else
		{
			return 4;
		}
	}

	public static void ApplyHaskellBuff(Character Knight, int Power)
	{
		Knight.Attack = Constants.TickAttack * (Knight.IndexAttack [Knight.CharacterIndex] + Power) + Constants.StandardAttack;
		Knight.Move = Knight.IndexMove [Knight.CharacterIndex] + Power;
		Knight.AppearWing();
	}

	public static void DeactivateCharacterColliders()
	{
		foreach(Character Knight in Characters)
		{
			if(Knight != null)
			{
				Knight.GetComponent<BoxCollider2D>().enabled = false;
			}
		}
	}

	public static void ActivateCharacterColliders()
	{
		foreach(Character Knight in Characters)
		{
			if(Knight != null)
			{
				Knight.GetComponent<BoxCollider2D>().enabled = true;
			}
		}
	}
}