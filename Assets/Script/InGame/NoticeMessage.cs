﻿using UnityEngine;
using System.Collections;
using TeamsAndCharacters;
using CommonConstants;

public class NoticeMessage : MonoBehaviour
{
	public Sprite GameStart;
	public Sprite MyTurnPintos;
	public Sprite MyTurnHaskell;
	public Sprite EnemyTurnPintos;
	public Sprite EnemyTurnHaskell;
	public Sprite GameEnd;

	void Start()
	{
		iTween.MoveTo(gameObject, iTween.Hash ("position", new Vector3(26.3f, 26.3f, -4), "Speed", Constants.MessageSpeed, "easeType", "linear", "oncompletetarget", gameObject, "oncomplete", "Restart"));
	}

	IEnumerator Restart()
	{
		yield return new WaitForSeconds (1);

		iTween.MoveTo(gameObject, iTween.Hash ("position", new Vector3(116.3f, 26.3f, -4), "Speed", Constants.MessageSpeed, "easeType", "linear", "oncompletetarget", gameObject, "oncomplete", "DestroyMyself"));
	}

	void DestroyMyself()
	{
		if(GetComponent<SpriteRenderer>().sprite == GameEnd)
		{
			Network.Disconnect(0);

			GameManager.ResetStaticVariables();
			CS_SelectedCharacters.ResetStaticVariables();

			BGMManager bgmPlayer = GameObject.FindObjectOfType(typeof(BGMManager)) as BGMManager;
			Destroy (bgmPlayer.gameObject);
			NetworkManager Manager = GameObject.FindObjectOfType (typeof(NetworkManager)) as NetworkManager;
			Destroy (Manager.gameObject);
			GameManager GM = GameObject.FindObjectOfType (typeof(GameManager)) as GameManager;
			Destroy (GM.gameObject);
			Application.LoadLevel("TitleScene");
		}
		else if(GetComponent<SpriteRenderer>().sprite == GameStart)
		{
			GameManager.ShowTurnMessage();
		}

		Destroy (gameObject);
	}
}