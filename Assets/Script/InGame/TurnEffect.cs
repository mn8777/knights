﻿using UnityEngine;
using System.Collections;

public class TurnEffect : MonoBehaviour 
{
	public Sprite Pintos;
	public Sprite Haskell;

	public bool forHost;

	public void DoSetting()
	{
		if(forHost == Network.isServer)
		{
			if(GameManager.Turn)
			{
				if(GameManager.myTeam == TeamsAndCharacters.Teams.Pintos)
				{
					GetComponent<SpriteRenderer>().sprite = Pintos;
				}
				else
				{
					GetComponent<SpriteRenderer>().sprite = Haskell;
				}
			}
			else
			{
				GetComponent<SpriteRenderer>().sprite = null;
			}
		}
		else
		{
			if(!GameManager.Turn)
			{
				if(GameManager.otherTeam == TeamsAndCharacters.Teams.Pintos)
				{
					GetComponent<SpriteRenderer>().sprite = Pintos;
				}
				else
				{
					GetComponent<SpriteRenderer>().sprite = Haskell;
				}
			}
			else
			{
				GetComponent<SpriteRenderer>().sprite = null;
			}
		}
	}
}