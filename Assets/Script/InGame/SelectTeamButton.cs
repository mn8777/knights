using UnityEngine;
using System.Collections;
using TeamsAndCharacters;

public class SelectTeamButton : MonoBehaviour 
{
	public Teams teamOfThisButton;

	void OnMouseDown()
	{
		GameManager.myTeam = teamOfThisButton;
		NetworkManager.SendTeamInfo (teamOfThisButton.ToString());
		Application.LoadLevel ("CharacterSelect");
	}
}