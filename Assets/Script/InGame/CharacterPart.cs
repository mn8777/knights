﻿using UnityEngine;
using System.Collections;

public class CharacterPart : MonoBehaviour
{
	public Sprite[] Sprites;

	public Character ParentCharacter;

	void Start()
	{
		GetComponent<SpriteRenderer> ().sprite = Sprites [ParentCharacter.CharacterIndex];
	}
}