﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamsAndCharacters;
using CommonConstants;

public class Tile : MonoBehaviour
{
	private AudioSource se;
	public AudioClip footStepSound;

	public GameManager Manager;

	public int X;
	public int Y;

	public Character CharacterOnTile;
	public Buff BuffOnTile;

	public SpriteRenderer MyRenderer;

	public Sprite DefaultSprite;
	public Sprite[] Tiles;
	public Sprite MovableSprite;
	public Sprite AttackableSprite;
	public Sprite MovableAndAttackableSprite;

	public bool Movable;

	public bool Occupied;

	public void Awake()
	{
		se = GetComponent<AudioSource>();
	}

	public void Start(){
		DefaultSprite = Tiles [Random.Range (0, 5)];
		MyRenderer.sprite = DefaultSprite;
	}

	public void OnMouseDown()
	{
		Character MovingCharacter = GameManager.MovingCharacter;
		if(Movable && BuffOnTile == null && !Occupied)
		{
			if(CharacterOnTile == null || CharacterOnTile == MovingCharacter)
			{
				MovingCharacter.startTile = MovingCharacter.CurrentTile;
				MovingCharacter.startDirection = MovingCharacter.Direction;
				MovingCharacter.startScaleX = MovingCharacter.transform.localScale.x;
				MovingCharacter.CurrentTile.CharacterOnTile = null;
				MovingCharacter.CurrentTile = this;

				iTween.MoveTo(MovingCharacter.gameObject, 
					iTween.Hash (
						"position", transform.position + Character.relativePosition_AboutTile(Y), 
						"Speed", Constants.MoveAniSpeed, 
						"easeType", "linear", 
						"oncompletetarget", this.gameObject, 
						"oncomplete", "AfterMoveAni")
					);
				MovingCharacter.GetComponent<Animator>().SetBool("Walk", true);

				se.PlayOneShot(footStepSound, 1.0F);

				CharacterOnTile = MovingCharacter;

				GameManager.DeactivateAllTiles();

				GameManager.ActivateCharacterColliders();
			}
		}
	}

	public void AfterMoveAni()
	{
		Character MovingCharacter = GameManager.MovingCharacter;

		MovingCharacter.TurnPhase = GameManager.TurnPhases.Direction;
		GameManager.GlobalTurnPhase = GameManager.TurnPhases.Direction;
		List<Tile> TileList = MovingCharacter.ShowAndReturn_AttackRange();

		MovingCharacter.GetComponent<Animator>().SetBool("Walk", false);

		se.Stop();
	}
}