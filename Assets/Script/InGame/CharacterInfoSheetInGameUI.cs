﻿using UnityEngine;
using System.Collections;
using CommonConstants;
using TeamsAndCharacters;

public class CharacterInfoSheetInGameUI : MonoBehaviour {
	
	public Sprite[] InfoSheets;
	public Sprite[] Background;
	public static int viewedInfoSheet=Constants.ofCharactersInEachTeam * 2;
	private int[] selectedCharacter = new int[Constants.ofTeamMembers*2];
	public int xAxisforPlayer1;
	public int xAxisforPlayer2;
	public int zAxis;
	private GameObject background;
	
	private SpriteRenderer sr;
	private SpriteRenderer backgroundSr;
	
	// Use this for initialization
	void Start () {
		background = GameObject.Find ("/InfoSheet/Background");
		backgroundSr = background.GetComponent<SpriteRenderer> ();
		backgroundSr.sprite = Background [0];
		sr = GetComponent<SpriteRenderer>();

		if (Network.isServer) {
			if (GameManager.myTeam == Teams.Haskell) {
				for (int i = 0; i <Constants.ofTeamMembers; i++) {
					selectedCharacter [i] = (GameManager.mySelectedCharacters [i]);
					Debug.Log (i + " " + selectedCharacter [i]);
				}
			}
			else if (GameManager.myTeam == Teams.Pintos) {
				for (int i = 0; i <Constants.ofTeamMembers; i++) {
					selectedCharacter [i] = (GameManager.mySelectedCharacters [i] + 10);
					Debug.Log (i + " " + selectedCharacter [i]);
				}			
			}

			if (GameManager.otherTeam == Teams.Haskell) {
				for(int i = Constants.ofTeamMembers; i<Constants.ofTeamMembers*2; i++){
					selectedCharacter[i]=(GameManager.otherSelectedCharacters[i-Constants.ofTeamMembers]);
					Debug.Log (i + " " + selectedCharacter[i]);
				}
			}
			else if (GameManager.otherTeam == Teams.Pintos) {
				for(int i = Constants.ofTeamMembers; i<Constants.ofTeamMembers*2; i++){
					selectedCharacter[i]=(GameManager.otherSelectedCharacters[i-Constants.ofTeamMembers] + 10);
					Debug.Log (i + " " + selectedCharacter[i]);
				}			
			}
		}

		else if (Network.isClient) {
			if (GameManager.otherTeam == Teams.Haskell) {
				for (int i = 0; i <Constants.ofTeamMembers; i++) {
					selectedCharacter [i] = (GameManager.otherSelectedCharacters [i]);
					Debug.Log (i + " " + selectedCharacter [i]);
				}
			}
			else if (GameManager.otherTeam == Teams.Pintos) {
				for (int i = 0; i <Constants.ofTeamMembers; i++) {
					selectedCharacter [i] = (GameManager.otherSelectedCharacters [i] + Constants.ofCharactersInEachTeam);
					Debug.Log (i + " " + selectedCharacter [i]);
				}			
			}
			
			if (GameManager.myTeam == Teams.Haskell) {
				for(int i = Constants.ofTeamMembers; i<Constants.ofTeamMembers*2; i++){
					selectedCharacter[i]=(GameManager.mySelectedCharacters[i-Constants.ofTeamMembers]);
					Debug.Log (i + " " + selectedCharacter[i]);
				}
			}
			else if (GameManager.myTeam == Teams.Pintos) {
				for(int i = Constants.ofTeamMembers; i<Constants.ofTeamMembers*2; i++){
					selectedCharacter[i]=(GameManager.mySelectedCharacters[i-Constants.ofTeamMembers] + Constants.ofCharactersInEachTeam);
					Debug.Log (i + " " + selectedCharacter[i]);
				}			
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(viewedInfoSheet<Constants.ofCharactersInEachTeam*2){
			
			sr.sprite=InfoSheets[selectedCharacter[viewedInfoSheet]];
			backgroundSr.sprite=Background[1];
			
			if(viewedInfoSheet<Constants.ofTeamMembers)
			{
				transform.position=new Vector3(xAxisforPlayer1,40-9*viewedInfoSheet,zAxis);
			}
			else if (viewedInfoSheet>=Constants.ofTeamMembers)
			{
				transform.position=new Vector3(xAxisforPlayer2,76-9*viewedInfoSheet,zAxis);
			}
			
		}
		else if(viewedInfoSheet==Constants.ofCharactersInEachTeam*2){
			sr.sprite=InfoSheets[Constants.ofCharactersInEachTeam*2];
			backgroundSr.sprite = Background [0];
		}
	}
}
